CREATE TABLE IF NOT EXISTS intoxicacao_db.02_atendimento (
	identificador_caso int,
	local_atendimento varchar(1024),
	complemento_local_atendimento varchar(1024),
	meio_atendimento varchar(1024),
	data_atendimento datetime
);
