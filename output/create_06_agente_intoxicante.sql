CREATE TABLE IF NOT EXISTS intoxicacao_db.06_agente_intoxicante (
	identificador_caso int,
	nome_popular_comercial varchar(1024),
	substancia_genero_especie varchar(1024),
	subclasse_agente varchar(1024),
	classe_agente varchar(1024),
	grupo_agente varchar(1024),
	parte_corpo varchar(1024),
	quantidade_agente varchar(1024),
	especificacao_quantidade_agente varchar(1024),
	dose_exposicao varchar(1024),
	especificacao_dose_exposicao varchar(1024)
);
