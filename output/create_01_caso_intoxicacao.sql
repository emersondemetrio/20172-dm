CREATE TABLE IF NOT EXISTS intoxicacao_db.01_caso_intoxicacao (
	identificador_caso int,
	data_entrada datetime,
	grupo_ficha_coletiva varchar(1024),
	tipo_atendimento varchar(1024),
	classificacao_gravidade varchar(1024),
	manifestacao_clinica varchar(1024),
	informacoes_adicionais_tratamento text,
	caso_revisado varchar(1024),
	caso_validado varchar(1024)
);
