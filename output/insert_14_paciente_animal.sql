INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'4568', 
	'', 
	'3', 
	'Meses', 
	'Masculino', 
	'1.000', 
	'', 
	'', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'4720', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'2.000', 
	'SÃO PEDRO DE ALCANTARA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'4722', 
	'', 
	'32', 
	'Anos', 
	'Masculino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'4724', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'4867', 
	'', 
	'', 
	'Ignorado', 
	'', 
	'', 
	'', 
	'', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'5698', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'7.000', 
	'CURITIBA', 
	'PR', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'5745', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'7.000', 
	'CURITIBA', 
	'PR', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'6343', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'3.000', 
	'', 
	'', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'6447', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Gato Doméstico SRD - Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'6481', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'30.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Boxer', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'6627', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'6634', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'6865', 
	'', 
	'27', 
	'Anos', 
	'', 
	'88.000', 
	'CAMPOS NOVOS', 
	'SC', 
	'BRASIL', 
	'', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'6866', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'7297', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'15.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'7770', 
	'', 
	'3', 
	'Anos', 
	'Masculino', 
	'10.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'8457', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'10.000', 
	'PORTO BELO', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'8584', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'20.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'8761', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'6.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'8762', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'4.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'9059', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Chihuahua', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'9172', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'', 
	'SÃO JOSÉ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'9243', 
	'', 
	'6', 
	'Meses', 
	'Ignorado', 
	'6.000', 
	'', 
	'', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pastor Alemão', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'9385', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'6.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Gato Doméstico SRD - Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'9416', 
	'', 
	'9', 
	'Anos', 
	'Masculino', 
	'30.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'10080', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'20.000', 
	'RIO DE JANEIRO', 
	'RJ', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'10095', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'35.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Dobermann', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'10824', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'3.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Siamês', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'10962', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'3.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'11080', 
	'', 
	'9', 
	'Meses', 
	'Masculino', 
	'20.000', 
	'PALHOÇA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Boxer', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'11083', 
	'1971-08-17', 
	'42', 
	'Anos', 
	'Feminino', 
	'', 
	'BALNEARIO CAMBORIU', 
	'SC', 
	'BRASIL', 
	'', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'11108', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'SÃO LEOPOLDO', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'11258', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'20.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'11987', 
	'', 
	'13', 
	'Anos', 
	'Masculino', 
	'25.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'American S. Terrier', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'12503', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'5.000', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Basset', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'12884', 
	'', 
	'7', 
	'Anos', 
	'Feminino', 
	'10.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'12886', 
	'', 
	'10', 
	'Anos', 
	'Feminino', 
	'20.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'12988', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'12997', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'18.800', 
	'SÃO JOSÉ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Border Collie', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'13284', 
	'2010-09-11', 
	'3', 
	'Anos', 
	'Masculino', 
	'35.000', 
	'BLUMENAU', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Boxer', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'13546', 
	'', 
	'3', 
	'Anos', 
	'Feminino', 
	'5.000', 
	'SÃO JOSÉ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'13605', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'5.000', 
	'SÃO JOSÉ', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Gato Doméstico SRD - Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'14906', 
	'', 
	'10', 
	'Anos', 
	'Feminino', 
	'20.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'14913', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'450.000', 
	'COCAL DO SUL', 
	'SC', 
	'BRASIL', 
	'Bovino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'14939', 
	'', 
	'6', 
	'Meses', 
	'Masculino', 
	'18.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pastor Alemão', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'15044', 
	'', 
	'8', 
	'Meses', 
	'Feminino', 
	'4.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Poodle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'15201', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'7.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'West White H. Terrier', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'15237', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'', 
	'SP', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'15238', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'3.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Gato Doméstico SRD - Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'15567', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'30.000', 
	'SÃO PEDRO DE ALCANTARA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'16134', 
	'', 
	'4', 
	'Meses', 
	'Masculino', 
	'1.500', 
	'JARAGUÁ DO SUL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'16248', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'18.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'16511', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'17042', 
	'', 
	'10', 
	'Meses', 
	'Masculino', 
	'6.500', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Schnauzer', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'17100', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'31.000', 
	'CHAPECÓ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pastor Belga', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'17182', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'10.000', 
	'BAGÉ', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'17260', 
	'', 
	'', 
	'', 
	'', 
	'2.300', 
	'JOAÇABA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'17458', 
	'', 
	'9', 
	'Meses', 
	'Masculino', 
	'45.000', 
	'MARCELÂNDIA', 
	'MT', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pit Bull', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'17895', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'SÃO PEDRO DE ALCANTARA', 
	'SC', 
	'BRASIL', 
	'', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'17956', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'20.000', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pit Bull', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'18066', 
	'', 
	'3', 
	'Anos', 
	'Feminino', 
	'40.000', 
	'LAPÃO', 
	'BA', 
	'BRASIL', 
	'Canino', 
	'', 
	'Labrador', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'18081', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Boxer', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'18090', 
	'', 
	'8', 
	'Meses', 
	'Ignorado', 
	'', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Lhasa Apso', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'18410', 
	'', 
	'3', 
	'Meses', 
	'Feminino', 
	'4.000', 
	'RIO DE JANEIRO', 
	'RJ', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'18681', 
	'', 
	'4', 
	'Anos', 
	'Feminino', 
	'20.000', 
	'GOVERNADOR CELSO RAMOS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'18771', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'PORTO ALEGRE', 
	'RS', 
	'BRASIL', 
	'Bovino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'19090', 
	'', 
	'7', 
	'Anos', 
	'Masculino', 
	'55.000', 
	'CIANORTE', 
	'PR', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'19399', 
	'', 
	'10', 
	'Anos', 
	'Feminino', 
	'2.950', 
	'CAXIAS DO SUL', 
	'RS', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Gato Doméstico SRD - Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'19411', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'', 
	'', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'19608', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'50.000', 
	'VIDEIRA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Boxer', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'19619', 
	'1987-05-30', 
	'26', 
	'Anos', 
	'Masculino', 
	'', 
	'JOAÇABA', 
	'SC', 
	'BRASIL', 
	'', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'19797', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'30.000', 
	'PALHOÇA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'20008', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'30.000', 
	'CAMPINAS', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Golden Retriever', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'20333', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'SÃO JOSÉ', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'20831', 
	'', 
	'8', 
	'Meses', 
	'Masculino', 
	'7.000', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Poodle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'21716', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'3.500', 
	'JARAGUÁ DO SUL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'22794', 
	'', 
	'2', 
	'Meses', 
	'Masculino', 
	'2.200', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Beagle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'22816', 
	'', 
	'3', 
	'Anos', 
	'Masculino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'22819', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'22830', 
	'', 
	'4', 
	'Anos', 
	'Feminino', 
	'5.000', 
	'NAVEGANTES', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'22906', 
	'', 
	'8', 
	'Meses', 
	'Masculino', 
	'13.000', 
	'', 
	'', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'23170', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'15.000', 
	'CURITIBA', 
	'PR', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'23317', 
	'', 
	'2', 
	'Meses', 
	'Masculino', 
	'2.500', 
	'BIGUAÇÚ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'23453', 
	'', 
	'7', 
	'Anos', 
	'Masculino', 
	'3.000', 
	'BALNEARIO CAMBORIU', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Maltês', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'23541', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'10.000', 
	'CAMBORIU', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'23739', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'', 
	'ARIQUEMES', 
	'RO', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'24051', 
	'2013-01-01', 
	'1', 
	'Anos', 
	'Masculino', 
	'9.600', 
	'INDAIAL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'24534', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'25599', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'3.000', 
	'JARAGUÁ DO SUL', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Gato Doméstico SRD - Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'25921', 
	'', 
	'', 
	'Ignorado', 
	'', 
	'3.000', 
	'BAURU', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'25929', 
	'', 
	'', 
	'', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'25943', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'25991', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'40.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Dogo Argentino', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'26189', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'4.200', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'26202', 
	'', 
	'3', 
	'Anos', 
	'Feminino', 
	'7.000', 
	'NOVA RESENDE', 
	'MG', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'26324', 
	'', 
	'4', 
	'Meses', 
	'Feminino', 
	'3.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'26541', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'', 
	'', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Gato Doméstico SRD - Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'27100', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Ave', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'27136', 
	'', 
	'3', 
	'Meses', 
	'', 
	'5.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'27137', 
	'', 
	'10', 
	'Meses', 
	'Feminino', 
	'7.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'27138', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'12.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'27204', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'35.000', 
	'AVARÉ', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pastor Alemão', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'28543', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'', 
	'SANTO ANTÔNIO DA PATRULHA', 
	'RS', 
	'BRASIL', 
	'Bovino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'28547', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'7.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'29014', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Maltês', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'29807', 
	'', 
	'5', 
	'Anos', 
	'Feminino', 
	'5.000', 
	'CHAPECÓ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'29876', 
	'', 
	'', 
	'', 
	'Masculino', 
	'30.000', 
	'SOROCABA', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pit Bull', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'29949', 
	'', 
	'4', 
	'Meses', 
	'Masculino', 
	'2.000', 
	'BLUMENAU', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'30101', 
	'2014-01-28', 
	'5', 
	'Meses', 
	'Masculino', 
	'18.000', 
	'BRASÍLIA', 
	'DF', 
	'BRASIL', 
	'Canino', 
	'', 
	'American S. Terrier', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'30507', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'10.000', 
	'SÃO PAULO', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'31678', 
	'', 
	'9', 
	'Meses', 
	'Masculino', 
	'35.000', 
	'GRAVATAÍ', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Labrador', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'31950', 
	'', 
	'3', 
	'Meses', 
	'Feminino', 
	'4.000', 
	'SÃO JOSÉ', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'32023', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'20.000', 
	'VIAMÃO', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'32061', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'35.000', 
	'BIGUAÇÚ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Labrador', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'33270', 
	'2012-02-03', 
	'2', 
	'Anos', 
	'Masculino', 
	'', 
	'JARAGUÁ DO SUL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'33469', 
	'', 
	'4', 
	'Meses', 
	'Feminino', 
	'10.000', 
	'SÃO JOÃO DEL REI', 
	'MG', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pastor Alemão', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'33628', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'70.000', 
	'', 
	'', 
	'BRASIL', 
	'Canino', 
	'', 
	'Dogue Alemão', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'34536', 
	'', 
	'5', 
	'Anos', 
	'Feminino', 
	'30.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pastor Alemão', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'34965', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'25.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'36006', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'30.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pastor Alemão', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'36482', 
	'', 
	'3', 
	'Meses', 
	'Masculino', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'36500', 
	'', 
	'', 
	'', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'36501', 
	'', 
	'', 
	'', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Poodle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'36878', 
	'', 
	'2', 
	'Anos', 
	'Feminino', 
	'25.000', 
	'PORTO ALEGRE', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'37617', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'6.000', 
	'PLANALTO', 
	'PR', 
	'BRASIL', 
	'Canino', 
	'', 
	'Poodle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'37813', 
	'', 
	'10', 
	'Anos', 
	'Masculino', 
	'25.000', 
	'ERECHIM', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Rottweiller', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'39320', 
	'', 
	'6', 
	'Meses', 
	'Masculino', 
	'15.000', 
	'JUIZ DE FORA', 
	'MG', 
	'BRASIL', 
	'Canino', 
	'', 
	'Chow Chow', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'39430', 
	'', 
	'4', 
	'Anos', 
	'Feminino', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'39461', 
	'', 
	'', 
	'', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'40110', 
	'', 
	'12', 
	'Anos', 
	'Feminino', 
	'35.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'40331', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'40549', 
	'', 
	'6', 
	'Meses', 
	'Feminino', 
	'', 
	'SÃO JOSÉ DO OURO', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'40672', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Animal selvagem/silvestre', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'41669', 
	'', 
	'7', 
	'Anos', 
	'Masculino', 
	'6.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Gato Doméstico SRD - Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'41921', 
	'', 
	'5', 
	'Anos', 
	'Masculino', 
	'30.000', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Boxer', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'42147', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'', 
	'', 
	'', 
	'BRASIL', 
	'Canino', 
	'', 
	'Boxer', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'42268', 
	'1957-07-12', 
	'57', 
	'Anos', 
	'Feminino', 
	'53.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Animal ignorado', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'42459', 
	'', 
	'5', 
	'Anos', 
	'Feminino', 
	'25.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'American S. Terrier', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'42460', 
	'', 
	'3', 
	'Anos', 
	'Masculino', 
	'17.000', 
	'SÃO DOMINGOS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pit Bull', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'42760', 
	'', 
	'3', 
	'Meses', 
	'Masculino', 
	'4.000', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'43076', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'43189', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'22.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'43366', 
	'', 
	'5', 
	'Anos', 
	'Feminino', 
	'6.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'43490', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'', 
	'CANELINHA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'43743', 
	'', 
	'13', 
	'Anos', 
	'', 
	'', 
	'ELDORADO DO SUL', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Labrador', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'43992', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'PORTO ALEGRE', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'44006', 
	'', 
	'', 
	'', 
	'Masculino', 
	'4.900', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Gato Doméstico SRD - Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'44942', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'45173', 
	'', 
	'7', 
	'Anos', 
	'Feminino', 
	'25.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'45550', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'3.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Chihuahua', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'45963', 
	'', 
	'', 
	'', 
	'Masculino', 
	'6.000', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'46017', 
	'', 
	'', 
	'', 
	'', 
	'6.000', 
	'BLUMENAU', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Lhasa Apso', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'46192', 
	'', 
	'3', 
	'Anos', 
	'Feminino', 
	'20.300', 
	'', 
	'', 
	'BRASIL', 
	'Canino', 
	'', 
	'Labrador', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'46325', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'46368', 
	'', 
	'', 
	'', 
	'', 
	'6.000', 
	'SÃO PAULO', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Golden Retriever', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'47194', 
	'', 
	'2', 
	'Meses', 
	'Masculino', 
	'8.450', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'47195', 
	'', 
	'2', 
	'Meses', 
	'Feminino', 
	'7.700', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'47196', 
	'', 
	'2', 
	'Meses', 
	'', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'47197', 
	'', 
	'2', 
	'Meses', 
	'', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'47198', 
	'', 
	'2', 
	'Meses', 
	'', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'47381', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'48759', 
	'2011-01-01', 
	'3', 
	'Anos', 
	'Feminino', 
	'2.170', 
	'INDAIAL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'48788', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'49159', 
	'', 
	'10', 
	'Anos', 
	'Masculino', 
	'40.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Boxer', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'49936', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'49966', 
	'', 
	'10', 
	'Anos', 
	'Masculino', 
	'', 
	'LAGES', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Poodle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'50338', 
	'', 
	'8', 
	'Meses', 
	'', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Gato Doméstico SRD - Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'51232', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'25.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'51412', 
	'', 
	'', 
	'', 
	'Ignorado', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'51583', 
	'', 
	'', 
	'', 
	'Masculino', 
	'27.000', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Sharpei', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'51665', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'10.000', 
	'RIO DO SUL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Beagle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'52110', 
	'2013-11-11', 
	'1', 
	'Anos', 
	'Masculino', 
	'', 
	'BAGÉ', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'52172', 
	'', 
	'3', 
	'Meses', 
	'Masculino', 
	'', 
	'CERQUEIRA CÉSAR', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'53838', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'5.000', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pequinês', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'55004', 
	'', 
	'6', 
	'Meses', 
	'Masculino', 
	'30.000', 
	'FEIRA DA MATA', 
	'BA', 
	'BRASIL', 
	'Canino', 
	'', 
	'Rottweiller', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'55005', 
	'', 
	'6', 
	'Meses', 
	'Feminino', 
	'12.000', 
	'FEIRA DE SANTANA', 
	'BA', 
	'BRASIL', 
	'Canino', 
	'', 
	'Rottweiller', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'55854', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'34.000', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pit Bull', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'55941', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'6.000', 
	'SÃO MIGUEL DO OESTE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'58327', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'20.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'58735', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'59359', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'59392', 
	'', 
	'', 
	'', 
	'Masculino', 
	'10.000', 
	'ARAQUARI', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'60915', 
	'', 
	'7', 
	'Meses', 
	'Feminino', 
	'15.000', 
	'SÃO PAULO', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'62435', 
	'', 
	'', 
	'', 
	'Feminino', 
	'2.800', 
	'SÃO JOSÉ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'63069', 
	'', 
	'6', 
	'Anos', 
	'Masculino', 
	'35.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Labrador', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'65230', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'10.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'66492', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'American S. Terrier', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'66893', 
	'', 
	'', 
	'', 
	'Feminino', 
	'26.500', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Weimaraner', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'68102', 
	'', 
	'15', 
	'Anos', 
	'Ignorado', 
	'', 
	'BAEPENDI', 
	'MG', 
	'BRASIL', 
	'Equino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'68378', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'', 
	'BALNEARIO CAMBORIU', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'68637', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'12.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'71500', 
	'', 
	'7', 
	'Anos', 
	'Feminino', 
	'20.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'72503', 
	'', 
	'3', 
	'Meses', 
	'Masculino', 
	'1.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'73754', 
	'', 
	'', 
	'', 
	'Feminino', 
	'25.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Akita', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'74498', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'74569', 
	'', 
	'6', 
	'Anos', 
	'Feminino', 
	'4.300', 
	'ITUPORANGA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'74800', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'75504', 
	'', 
	'3', 
	'Anos', 
	'Ignorado', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Ave', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'76035', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'77363', 
	'', 
	'4', 
	'Anos', 
	'Masculino', 
	'3.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'79554', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'BRACO DO NORTE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'79649', 
	'', 
	'6', 
	'Anos', 
	'Masculino', 
	'', 
	'ERECHIM', 
	'RS', 
	'BRASIL', 
	'Equino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'79871', 
	'', 
	'7', 
	'Anos', 
	'Masculino', 
	'40.000', 
	'CRUZ ALTA', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'82034', 
	'', 
	'10', 
	'Anos', 
	'Masculino', 
	'50.000', 
	'ARARAQUARA', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'82337', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'6.000', 
	'JUIZ DE FORA', 
	'MG', 
	'BRASIL', 
	'Canino', 
	'', 
	'Poodle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'82764', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'CHAPECÓ', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Gato Doméstico SRD - Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'83343', 
	'', 
	'6', 
	'Anos', 
	'Masculino', 
	'32.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'American S. Terrier', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'84306', 
	'', 
	'4', 
	'Anos', 
	'Feminino', 
	'7.000', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'85353', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'', 
	'SÃO JOSÉ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'86627', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'1.000', 
	'FORTALEZA', 
	'CE', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'87599', 
	'', 
	'13', 
	'Anos', 
	'Feminino', 
	'13.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'87838', 
	'', 
	'2', 
	'Meses', 
	'Masculino', 
	'1.500', 
	'GAROPABA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'88401', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'10.000', 
	'ARAQUARI', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'88906', 
	'', 
	'2', 
	'Anos', 
	'Feminino', 
	'3.500', 
	'SÃO JOSÉ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'89048', 
	'', 
	'6', 
	'Meses', 
	'Feminino', 
	'', 
	'PALHOÇA', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Persa', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'89092', 
	'', 
	'14', 
	'Anos', 
	'Masculino', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Dachshund', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'89892', 
	'', 
	'14', 
	'Anos', 
	'Masculino', 
	'', 
	'LONDRINA', 
	'PR', 
	'BRASIL', 
	'Canino', 
	'', 
	'Poodle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'90262', 
	'', 
	'', 
	'', 
	'Feminino', 
	'', 
	'BLUMENAU', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'90473', 
	'2004-01-01', 
	'11', 
	'Anos', 
	'Masculino', 
	'', 
	'CRICIÚMA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pit Bull', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'90559', 
	'', 
	'', 
	'', 
	'Masculino', 
	'15.000', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'91678', 
	'2015-02-02', 
	'4', 
	'Meses', 
	'Masculino', 
	'5.000', 
	'INDAIAL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'92067', 
	'', 
	'6', 
	'Anos', 
	'Masculino', 
	'1.000', 
	'', 
	'', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'93372', 
	'', 
	'11', 
	'Meses', 
	'Feminino', 
	'', 
	'INDAIAL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'94492', 
	'', 
	'3', 
	'Anos', 
	'Masculino', 
	'13.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Beagle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'94914', 
	'', 
	'6', 
	'Meses', 
	'Feminino', 
	'3.000', 
	'PORTO UNIAO', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Lhasa Apso', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'94964', 
	'', 
	'7', 
	'Meses', 
	'Masculino', 
	'10.000', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'95059', 
	'', 
	'6', 
	'Meses', 
	'Feminino', 
	'', 
	'BAGÉ', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Buldogue Inglês', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'96134', 
	'2015-02-06', 
	'4', 
	'Meses', 
	'Masculino', 
	'18.000', 
	'RIO DE JANEIRO', 
	'RJ', 
	'BRASIL', 
	'Canino', 
	'', 
	'Golden Retriever', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'97678', 
	'', 
	'2', 
	'Anos', 
	'Feminino', 
	'3.000', 
	'JOAÇABA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'99729', 
	'', 
	'13', 
	'Anos', 
	'Feminino', 
	'', 
	'MARÍLIA', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Poodle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'99797', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'6.000', 
	'INDAIAL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'101971', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'20.000', 
	'UMUARAMA', 
	'PR', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'102219', 
	'', 
	'', 
	'', 
	'Feminino', 
	'300.000', 
	'CAXAMBU', 
	'MG', 
	'BRASIL', 
	'Bovino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'104944', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'', 
	'SÃO JOSÉ DO RIO PRETO', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'105781', 
	'', 
	'5', 
	'Anos', 
	'Feminino', 
	'400.000', 
	'APORÁ', 
	'BA', 
	'BRASIL', 
	'Bovino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'106521', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'200.000', 
	'ITAJAÍ', 
	'SC', 
	'BRASIL', 
	'Bovino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'107199', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'6.000', 
	'BRASÍLIA', 
	'DF', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'107645', 
	'', 
	'2', 
	'Anos', 
	'Feminino', 
	'35.000', 
	'', 
	'MG', 
	'BRASIL', 
	'Canino', 
	'', 
	'Buldogue Inglês', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'110218', 
	'', 
	'4', 
	'Meses', 
	'Masculino', 
	'4.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'111979', 
	'', 
	'', 
	'', 
	'Feminino', 
	'6.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'111980', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'17.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'112296', 
	'', 
	'', 
	'', 
	'Masculino', 
	'5.000', 
	'TENENTE PORTELA', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'112506', 
	'', 
	'80', 
	'Anos', 
	'Masculino', 
	'', 
	'APODI', 
	'RN', 
	'BRASIL', 
	'', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'113296', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'116962', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'', 
	'', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'117389', 
	'', 
	'2', 
	'Anos', 
	'Feminino', 
	'5.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'121163', 
	'', 
	'4', 
	'Meses', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'123354', 
	'', 
	'', 
	'', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'124001', 
	'', 
	'8', 
	'Meses', 
	'Feminino', 
	'', 
	'GRAVATAL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Dogue Alemão', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'125213', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'11.000', 
	'SÃO MATEUS DO SUL', 
	'PR', 
	'BRASIL', 
	'Canino', 
	'', 
	'Beagle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'125430', 
	'', 
	'', 
	'', 
	'Feminino', 
	'3.400', 
	'CAMBORIU', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Poodle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'126120', 
	'', 
	'', 
	'', 
	'Masculino', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'126152', 
	'', 
	'12', 
	'Anos', 
	'Masculino', 
	'14.000', 
	'NOVO HAMBURGO', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'126577', 
	'', 
	'2', 
	'Anos', 
	'Feminino', 
	'20.000', 
	'BIGUAÇÚ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Chow Chow', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'128663', 
	'', 
	'10', 
	'Anos', 
	'Feminino', 
	'27.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Boxer', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'128868', 
	'', 
	'', 
	'', 
	'Ignorado', 
	'', 
	'IMBITUBA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'128904', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'15.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'129353', 
	'', 
	'2', 
	'Anos', 
	'', 
	'', 
	'SÃO PEDRO DE ALCANTARA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'130291', 
	'1999-02-04', 
	'16', 
	'Anos', 
	'Feminino', 
	'67.000', 
	'CURITIBANOS', 
	'SC', 
	'BRASIL', 
	'', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'130729', 
	'', 
	'7', 
	'Anos', 
	'Masculino', 
	'20.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'130981', 
	'', 
	'6', 
	'Anos', 
	'Masculino', 
	'', 
	'URUBICI', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'131799', 
	'', 
	'3', 
	'Anos', 
	'Feminino', 
	'2.000', 
	'SÃO GABRIEL', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'132372', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'132827', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'', 
	'IMBITUBA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'134219', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'7.000', 
	'PORTO ALEGRE', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'134377', 
	'', 
	'', 
	'', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'135409', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'3.000', 
	'IÇARA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'135691', 
	'', 
	'16', 
	'Anos', 
	'Masculino', 
	'4.000', 
	'RIO DE JANEIRO', 
	'RJ', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'136060', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'2.000', 
	'IRANI', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'136962', 
	'', 
	'3', 
	'Meses', 
	'Masculino', 
	'2.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'141034', 
	'', 
	'2', 
	'Meses', 
	'Feminino', 
	'', 
	'SANTO AMARO DA IMPERATRIZ', 
	'SC', 
	'BRASIL', 
	'Equino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'143145', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'144461', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'148501', 
	'', 
	'', 
	'', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'150245', 
	'', 
	'', 
	'', 
	'Masculino', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'150512', 
	'', 
	'', 
	'', 
	'Feminino', 
	'', 
	'AMERICANA', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'151930', 
	'', 
	'', 
	'', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'153989', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'3.000', 
	'GAROPABA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'154449', 
	'', 
	'3', 
	'Anos', 
	'Feminino', 
	'35.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Dobermann', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'155641', 
	'', 
	'', 
	'', 
	'', 
	'2.500', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'156501', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'5.000', 
	'RIO DE JANEIRO', 
	'RJ', 
	'BRASIL', 
	'Canino', 
	'', 
	'Maltês', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'156665', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'156766', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'', 
	'SERRA GRANDE', 
	'PB', 
	'BRASIL', 
	'Canino', 
	'', 
	'Samoieda', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'158777', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'40.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pit Bull', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'159842', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'161190', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pug', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'161442', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Equino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'162174', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'SÃO JOSÉ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'163092', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'1.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'164048', 
	'', 
	'4', 
	'Anos', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Persa', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'164174', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'', 
	'SÃO PAULO', 
	'SP', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Siamês', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'164515', 
	'', 
	'12', 
	'Anos', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'165618', 
	'', 
	'3', 
	'Anos', 
	'Feminino', 
	'2.000', 
	'PETRÓPOLIS', 
	'RJ', 
	'BRASIL', 
	'Canino', 
	'', 
	'Basset', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'165684', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'7.000', 
	'CAXIAS DO SUL', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Basset', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'165786', 
	'', 
	'4', 
	'Anos', 
	'Feminino', 
	'5.000', 
	'ITAÚNA', 
	'MG', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'165996', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Husky Siberiano', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'166369', 
	'', 
	'', 
	'', 
	'Masculino', 
	'', 
	'GAROPABA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'166606', 
	'2011-03-16', 
	'5', 
	'Anos', 
	'Masculino', 
	'6.000', 
	'FREDERICO WESTPHALEN', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Lhasa Apso', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'167713', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'', 
	'POMERODE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'168407', 
	'', 
	'8', 
	'Anos', 
	'Feminino', 
	'8.000', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Dachshund', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'170337', 
	'', 
	'2', 
	'Anos', 
	'Ignorado', 
	'15.000', 
	'SÃO JOSÉ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'171262', 
	'', 
	'4', 
	'Anos', 
	'Masculino', 
	'', 
	'PORTO ALEGRE', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'171264', 
	'', 
	'', 
	'', 
	'Masculino', 
	'', 
	'INDAIAL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'172122', 
	'', 
	'9', 
	'Meses', 
	'Masculino', 
	'40.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Rottweiller', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'173158', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'1.000', 
	'', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'174742', 
	'', 
	'3', 
	'Anos', 
	'Masculino', 
	'400.000', 
	'IPERÓ', 
	'SP', 
	'BRASIL', 
	'Equino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'175017', 
	'', 
	'6', 
	'Meses', 
	'Masculino', 
	'', 
	'PALHOÇA', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'175071', 
	'', 
	'8', 
	'Anos', 
	'Feminino', 
	'', 
	'INDAIAL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'176258', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'5.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'177154', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'3.000', 
	'RIO DE JANEIRO', 
	'RJ', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'178116', 
	'', 
	'8', 
	'Meses', 
	'Masculino', 
	'15.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Beagle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'178300', 
	'', 
	'9', 
	'Anos', 
	'Masculino', 
	'11.000', 
	'PORTO ALEGRE', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Fox Paulistinha', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'179356', 
	'', 
	'4', 
	'Meses', 
	'', 
	'3.000', 
	'INDAIAL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'179988', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'', 
	'SÃO JOSÉ', 
	'SC', 
	'BRASIL', 
	'Equino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'182595', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'5.000', 
	'RIO DE JANEIRO', 
	'RJ', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'182707', 
	'', 
	'1', 
	'Anos', 
	'Feminino', 
	'', 
	'UBERLÂNDIA', 
	'MG', 
	'BRASIL', 
	'Canino', 
	'', 
	'Labrador', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'183911', 
	'2016-01-27', 
	'4', 
	'Meses', 
	'Feminino', 
	'3.500', 
	'MARICÁ', 
	'RJ', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'184909', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'14.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'185698', 
	'', 
	'', 
	'', 
	'Feminino', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'186729', 
	'', 
	'5', 
	'Meses', 
	'Ignorado', 
	'5.000', 
	'SENHOR DO BONFIM', 
	'BA', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'192286', 
	'', 
	'5', 
	'Meses', 
	'', 
	'', 
	'TUBARÃO', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Lhasa Apso', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'192512', 
	'', 
	'11', 
	'Meses', 
	'Masculino', 
	'4.000', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'193133', 
	'', 
	'4', 
	'Meses', 
	'Feminino', 
	'13.000', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Bull Terrier', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'194565', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Buldogue Inglês', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'195035', 
	'', 
	'', 
	'', 
	'Feminino', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'196577', 
	'', 
	'3', 
	'Meses', 
	'Feminino', 
	'5.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'197158', 
	'', 
	'3', 
	'Anos', 
	'Feminino', 
	'30.000', 
	'ERECHIM', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Boxer', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'198722', 
	'', 
	'', 
	'', 
	'Masculino', 
	'15.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'198723', 
	'', 
	'', 
	'', 
	'Masculino', 
	'10.000', 
	'SANTANA DE PARNAÍBA', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'198961', 
	'', 
	'', 
	'', 
	'Feminino', 
	'6.300', 
	'SÃO JOSÉ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Lhasa Apso', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'200713', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'4.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'202558', 
	'', 
	'6', 
	'Meses', 
	'Feminino', 
	'', 
	'SÃO JOSÉ DOS CAMPOS', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'202559', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'', 
	'SÃO JOSÉ DOS CAMPOS', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'202991', 
	'', 
	'3', 
	'Meses', 
	'Masculino', 
	'', 
	'LAGES', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Labrador', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'208150', 
	'', 
	'3', 
	'Anos', 
	'Masculino', 
	'8.000', 
	'BRASÍLIA', 
	'DF', 
	'BRASIL', 
	'Canino', 
	'', 
	'Lhasa Apso', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'209587', 
	'', 
	'', 
	'', 
	'Masculino', 
	'', 
	'', 
	'', 
	'BRASIL', 
	'Ave', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'209836', 
	'', 
	'8', 
	'Meses', 
	'Masculino', 
	'20.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'211540', 
	'2014-09-01', 
	'2', 
	'Anos', 
	'Masculino', 
	'4.200', 
	'CHAPECÓ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Poodle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'214129', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'15.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'215219', 
	'', 
	'', 
	'', 
	'Feminino', 
	'40.000', 
	'BIGUAÇÚ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Labrador', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'216132', 
	'', 
	'', 
	'', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'219268', 
	'', 
	'5', 
	'Anos', 
	'Feminino', 
	'4.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'220327', 
	'', 
	'', 
	'', 
	'Masculino', 
	'3.000', 
	'', 
	'', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'222510', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'35.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Labrador', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'223332', 
	'', 
	'', 
	'', 
	'Masculino', 
	'30.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'227534', 
	'', 
	'3', 
	'Meses', 
	'Masculino', 
	'3.000', 
	'PASSO FUNDO', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'228597', 
	'', 
	'2', 
	'Anos', 
	'Feminino', 
	'25.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'American S. Terrier', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'230162', 
	'', 
	'4', 
	'Anos', 
	'Feminino', 
	'12.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'230321', 
	'', 
	'2', 
	'Anos', 
	'Masculino', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'231832', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'', 
	'PALHOÇA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'232632', 
	'', 
	'7', 
	'Anos', 
	'Feminino', 
	'', 
	'BLUMENAU', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Lhasa Apso', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'232633', 
	'', 
	'6', 
	'Meses', 
	'Masculino', 
	'', 
	'BLUMENAU', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'233583', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'234628', 
	'', 
	'', 
	'', 
	'Feminino', 
	'12.000', 
	'CUIABÁ', 
	'MT', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'235340', 
	'', 
	'4', 
	'Anos', 
	'Masculino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pit Bull', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'235717', 
	'', 
	'2', 
	'Meses', 
	'', 
	'', 
	'', 
	'', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pastor Alemão', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'235944', 
	'', 
	'5', 
	'Anos', 
	'Masculino', 
	'6.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Poodle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'237363', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'50.000', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Golden Retriever', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'242079', 
	'', 
	'', 
	'', 
	'Feminino', 
	'2.000', 
	'CAXIAS DO SUL', 
	'RS', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'242153', 
	'', 
	'4', 
	'Anos', 
	'Feminino', 
	'', 
	'SANTA MARIA', 
	'RS', 
	'BRASIL', 
	'', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'242557', 
	'', 
	'5', 
	'Anos', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'243793', 
	'', 
	'2', 
	'Meses', 
	'Feminino', 
	'1.000', 
	'NOVA TRENTO', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'247613', 
	'', 
	'8', 
	'Anos', 
	'Feminino', 
	'7.000', 
	'BLUMENAU', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Schnauzer', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'248106', 
	'', 
	'', 
	'', 
	'Masculino', 
	'10.000', 
	'RIO DE JANEIRO', 
	'RJ', 
	'BRASIL', 
	'Canino', 
	'', 
	'Dachshund', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'248375', 
	'', 
	'5', 
	'Anos', 
	'Masculino', 
	'', 
	'BRASÍLIA', 
	'DF', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'249447', 
	'', 
	'2', 
	'Anos', 
	'Ignorado', 
	'3.000', 
	'PARÁ DE MINAS', 
	'MG', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'249665', 
	'', 
	'4', 
	'Meses', 
	'Masculino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'250280', 
	'', 
	'13', 
	'Anos', 
	'Masculino', 
	'10.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'251088', 
	'2014-07-17', 
	'2', 
	'Anos', 
	'Masculino', 
	'5.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Yorkshire', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'251096', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'251195', 
	'', 
	'', 
	'', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'251461', 
	'', 
	'2', 
	'Anos', 
	'Feminino', 
	'3.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Poodle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'251649', 
	'', 
	'4', 
	'Meses', 
	'Feminino', 
	'', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Rottweiller', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'251784', 
	'', 
	'', 
	'', 
	'', 
	'30.000', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pastor Alemão', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'253156', 
	'', 
	'8', 
	'Anos', 
	'Feminino', 
	'25.000', 
	'INDAIAL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'253274', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Labrador', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'254234', 
	'', 
	'', 
	'', 
	'Feminino', 
	'2.000', 
	'BLUMENAU', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'255916', 
	'', 
	'3', 
	'Anos', 
	'Feminino', 
	'20.000', 
	'PALHOÇA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'256762', 
	'', 
	'', 
	'', 
	'Feminino', 
	'22.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'257212', 
	'', 
	'9', 
	'Anos', 
	'Masculino', 
	'10.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'257244', 
	'', 
	'4', 
	'Meses', 
	'Feminino', 
	'2.000', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'257298', 
	'', 
	'5', 
	'Anos', 
	'Masculino', 
	'10.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Beagle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'257732', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'1.500', 
	'CANELA', 
	'RS', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'258040', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'258081', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'259436', 
	'', 
	'17', 
	'Anos', 
	'Feminino', 
	'17.000', 
	'MARICÁ', 
	'RJ', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pit Bull', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'259956', 
	'', 
	'3', 
	'Anos', 
	'Masculino', 
	'23.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Boxer', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'260305', 
	'', 
	'8', 
	'Anos', 
	'Feminino', 
	'3.500', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'261333', 
	'', 
	'2', 
	'Meses', 
	'Masculino', 
	'6.000', 
	'EREBANGO', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Golden Retriever', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'262472', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'21.000', 
	'RIO DE JANEIRO', 
	'RJ', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'262800', 
	'', 
	'3', 
	'Anos', 
	'Masculino', 
	'25.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pointer', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'264731', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'264732', 
	'', 
	'4', 
	'Meses', 
	'Masculino', 
	'1.900', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Gato Doméstico SRD - Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'265207', 
	'', 
	'', 
	'', 
	'Masculino', 
	'', 
	'GOIÂNIA', 
	'GO', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'265573', 
	'', 
	'6', 
	'Anos', 
	'Feminino', 
	'10.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pug', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'266052', 
	'', 
	'2', 
	'Anos', 
	'Feminino', 
	'', 
	'VIDEIRA', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Gato Doméstico SRD - Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'267745', 
	'2016-01-01', 
	'1', 
	'Anos', 
	'Feminino', 
	'3.000', 
	'SÃO PEDRO DE ALCANTARA', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'268825', 
	'', 
	'4', 
	'Meses', 
	'Masculino', 
	'', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pit Bull', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'269086', 
	'', 
	'3', 
	'Anos', 
	'Masculino', 
	'', 
	'BLUMENAU', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'270156', 
	'', 
	'7', 
	'Meses', 
	'Feminino', 
	'2.500', 
	'BLUMENAU', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'270789', 
	'', 
	'5', 
	'Meses', 
	'Masculino', 
	'15.000', 
	'SÃO PAULO', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Labrador', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'271130', 
	'', 
	'', 
	'', 
	'Feminino', 
	'4.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'271347', 
	'', 
	'4', 
	'Anos', 
	'Feminino', 
	'14.000', 
	'CRICIÚMA', 
	'SC', 
	'BRASIL', 
	'', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'271374', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'272046', 
	'', 
	'', 
	'', 
	'Feminino', 
	'50.000', 
	'FRAIBURGO', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Labrador', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'272065', 
	'', 
	'7', 
	'Anos', 
	'Masculino', 
	'40.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'273062', 
	'2016-07-01', 
	'8', 
	'Meses', 
	'Ignorado', 
	'', 
	'INDAIAL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'273786', 
	'', 
	'8', 
	'Anos', 
	'Ignorado', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'274066', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'', 
	'', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'274892', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'30.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'276258', 
	'', 
	'2', 
	'Anos', 
	'Feminino', 
	'5.200', 
	'INDAIAL', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Shih Tzu', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'276272', 
	'', 
	'', 
	'', 
	'Feminino', 
	'30.000', 
	'', 
	'PR', 
	'BRASIL', 
	'Canino', 
	'', 
	'Rottweiller', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'277607', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'20.000', 
	'GOIÂNIA', 
	'GO', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'278128', 
	'', 
	'2', 
	'Meses', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'278952', 
	'', 
	'7', 
	'Anos', 
	'Masculino', 
	'45.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'281450', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'', 
	'SC', 
	'BRASIL', 
	'Felino doméstico (gato)', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'283408', 
	'', 
	'2', 
	'Anos', 
	'Feminino', 
	'24.000', 
	'JOINVILLE', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pointer', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'283469', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'283585', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'283838', 
	'2015-12-01', 
	'1', 
	'Anos', 
	'Ignorado', 
	'2.000', 
	'CAMPINAS', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'288382', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'', 
	'', 
	'', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'289076', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'10.000', 
	'SANTA MARIA DO HERVAL', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'291512', 
	'', 
	'', 
	'', 
	'Masculino', 
	'', 
	'RIO DE JANEIRO', 
	'RJ', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'291604', 
	'', 
	'', 
	'', 
	'Masculino', 
	'', 
	'PASSO FUNDO', 
	'RS', 
	'BRASIL', 
	'Canino', 
	'', 
	'Beagle', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'291912', 
	'', 
	'3', 
	'Anos', 
	'', 
	'', 
	'JOAÇABA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Labrador', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'293696', 
	'', 
	'', 
	'Ignorado', 
	'Feminino', 
	'16.000', 
	'BIGUAÇÚ', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Chow Chow', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'295141', 
	'', 
	'', 
	'Ignorado', 
	'Ignorado', 
	'', 
	'VALINHOS', 
	'SP', 
	'BRASIL', 
	'Canino', 
	'', 
	'Ignorada', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'297725', 
	'', 
	'2', 
	'Anos', 
	'Feminino', 
	'5.000', 
	'BALNEARIO CAMBORIU', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Pinscher', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'298846', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'1.000', 
	'PARAÍ', 
	'RS', 
	'BRASIL', 
	'Animal selvagem/silvestre', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'300070', 
	'', 
	'8', 
	'Anos', 
	'Masculino', 
	'18.000', 
	'JOAÇABA', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Cão SRD – Sem Raça Definida', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'302818', 
	'', 
	'', 
	'Ignorado', 
	'Masculino', 
	'15.000', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'303842', 
	'', 
	'1', 
	'Anos', 
	'Masculino', 
	'3.000', 
	'BALNEARIO CAMBORIU', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'Outra', 
	''
);

INSERT INTO intoxicacao_db.14_paciente_animal (
	identificador_caso, 
	data_nascimento, 
	idade, 
	especificacao_idade, 
	sexo, 
	peso, 
	cidade_residencia, 
	estado_residencia, 
	pais_residencia, 
	especie, 
	complemento_especie, 
	raca, 
	complemento_raca
) VALUES (
	'307887', 
	'', 
	'', 
	'', 
	'Feminino', 
	'', 
	'FLORIANÓPOLIS', 
	'SC', 
	'BRASIL', 
	'Canino', 
	'', 
	'', 
	''
);

