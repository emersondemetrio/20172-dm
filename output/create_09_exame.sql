CREATE TABLE IF NOT EXISTS intoxicacao_db.09_exame (
	identificador_caso int,
	descricao_tipo_exame varchar(1024),
	descricao_modalidade varchar(1024),
	tipo_resultado varchar(1024),
	valor_referencia_inferior varchar(1024),
	valor_referencia_superior varchar(1024),
	valor_referencia_textual varchar(1024),
	unidade_medida varchar(1024),
	especificacao_resultado_qualitativo varchar(1024),
	tipo_amostra varchar(1024),
	data_exame datetime,
	resultado_quantitativo varchar(1024),
	desfecho_resultado_quantitativo varchar(1024),
	resultado_qualitativo varchar(1024),
	resultado_textual varchar(1024),
	complemento varchar(1024)
);
