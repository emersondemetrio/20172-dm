INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'4487', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'4578', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'4609', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5032', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5159', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5159', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5187', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5353', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5411', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5561', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5604', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5735', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Informação sobre riscos e controle de caramujo africano.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5771', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Informação sobre casos de cães vítimas de caramujo africano.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5802', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5802', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5822', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5955', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'5955', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6236', 
	'Informação Ocupacional', 
	'Dados técnicos sobre segurança e manuseio de material (ficha técnica)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6273', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6273', 
	'Informação Toxicológica', 
	'Condutas terapêuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6274', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6401', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6401', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6414', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6422', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6601', 
	'Metais', 
	'Informações sobre metais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6629', 
	'Outros', 
	'Informações sobre outros intoxicantes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6691', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6745', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Informações de medicações que podem e/ou não podem ser usadas em criança de 6 anos com frutosemia. Informações sobre medicações de uso mais geral e corriqueiro, como analgésicos, antinflamatórios, antieméticos, etc.
Procurei exaustivamente na web, não encontrei.
Outra coisa seria um site de informações para pais e cuidadores.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6749', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6778', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'6943', 
	'Produtos de Uso Veterinário', 
	'Informações sobre produtos de uso veterinário', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7478', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7567', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7575', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7594', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7758', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7773', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7782', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7918', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7923', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'7940', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8065', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8097', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8125', 
	'Produtos de Uso Veterinário', 
	'Informações sobre produtos de uso veterinário', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8128', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8132', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8299', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8579', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8601', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8760', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8773', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8900', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8957', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8962', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8967', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'8974', 
	'Identificação de Substâncias', 
	'Identificação de formas farmacêuticas sólidas (sob prescrição, isentas de prescrição, suplemento dietético/ervas, ilícita)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9055', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9227', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9231', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9244', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9289', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9289', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9712', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9712', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9763', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9855', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9875', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9903', 
	'Drogas de Abuso', 
	'Triagem toxicológica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'9992', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10145', 
	'Medicamentos', 
	'Administração de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10192', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10345', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10365', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Recolhimento de animais'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10495', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10555', 
	'Drogas de Abuso', 
	'Efeitos de substâncias ilícitas - sem vítima(s) identificada(s)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10704', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10705', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10777', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10907', 
	'Informação Ocupacional', 
	'Informações sobre os produtos químicos no local de trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10923', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'10980', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'11220', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Enfermeira gostaria de saber se depois de quase 4 meses  de uma picada de aranha marrom, o veneno do animal pode causar hemólise. Há um caso assim no município e esquipe está em dúvida; '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'11402', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'11412', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'11904', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'11907', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'11954', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12084', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12130', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12130', 
	'Administrativa', 
	'Informações sobre o Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12130', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12141', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12353', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12458', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'12975', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13114', 
	'Medicamentos', 
	'Administração de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13119', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'estatística dos casos atendidos no CIT'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13221', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13221', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13279', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13306', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13307', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13347', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13347', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13371', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13377', 
	'Medicamentos', 
	'Uso de medicamentos durante a gravidez', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13441', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'13444', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'14210', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'14216', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'14221', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'14370', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'14412', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15040', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15236', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15265', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15364', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15367', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15392', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15662', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15746', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15793', 
	'Inseticidas de Uso Doméstico', 
	'Informações sobre inseticidas de uso doméstico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15797', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Solicitação de panfletos informativos para realização de atividade de prevenção.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15802', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15802', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15927', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	'Monografia de Etanol e Metanol'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15927', 
	'Identificação de Substâncias', 
	'Identificação de substâncias geralmente envolvidas em abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'15943', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16007', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16412', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16412', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16418', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16473', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16780', 
	'Drogas de Abuso', 
	'Triagem toxicológica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16984', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16986', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'16995', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'17110', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'17213', 
	'Identificação de Substâncias', 
	'Identificação de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'17946', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18083', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18230', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18234', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18234', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18279', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18296', 
	'Informação Toxicológica', 
	'Doses tóxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18316', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18788', 
	'Informação Toxicológica', 
	'Análise toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18790', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18849', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18920', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Solicita informações sobre risco de picada de aranha para lactante.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18926', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18930', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'18930', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19113', 
	'Metais', 
	'Informações sobre metais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19297', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19400', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19400', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19405', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19423', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19449', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19455', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19459', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19465', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19472', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19806', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19827', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19827', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19831', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19940', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'19942', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20034', 
	'Informação Toxicológica', 
	'Prevenção/Segurança/Informações educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20202', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20256', 
	'Medicamentos', 
	'Monitorização terapêutica de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20683', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20688', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20708', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20716', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20764', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20847', 
	'Medicamentos', 
	'Reações adversas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'20927', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21039', 
	'Administrativa', 
	'Informações sobre o Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21058', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21058', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21325', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21335', 
	'Produtos Domissanitários', 
	'Informações sobre produtos domissanitários', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21358', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21465', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21470', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21470', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21514', 
	'Medicamentos', 
	'Monitorização terapêutica de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21521', 
	'Informação Ocupacional', 
	'Informação sobre Toxicologia Ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21833', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'21833', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22091', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22226', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22341', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22352', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22352', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22360', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22381', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22420', 
	'Informação Ocupacional', 
	'Informação sobre Toxicologia Ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22448', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22448', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22448', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22448', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22503', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22520', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22520', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'22899', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23308', 
	'Medicamentos', 
	'Indicações/Uso terapêutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23313', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Dose Tóxica'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23378', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23378', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23379', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23382', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23929', 
	'Plantas e Fungos', 
	'Identificação de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'23942', 
	'Alimentos', 
	'Intoxicação alimentar - sem vítima(s) identificada(2)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24010', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24010', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24035', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24135', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'risco de toxicidade'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24157', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24223', 
	'Informação Ocupacional', 
	'Informações sobre os produtos químicos no local de trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24275', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24385', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24385', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24397', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24489', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24489', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24502', 
	'Metais', 
	'Informações sobre metais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24570', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24633', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24633', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24633', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24701', 
	'Outros', 
	'Informação sobre doenças gerais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24737', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24737', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24768', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24775', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'24873', 
	'Drogas de Abuso', 
	'Informações sobre drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25081', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25165', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25263', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25267', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25430', 
	'Administrativa', 
	'Empréstimo de material didático', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25430', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25692', 
	'Inseticidas de Uso Doméstico', 
	'Informações sobre inseticidas de uso doméstico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25758', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25758', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25758', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25763', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25763', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25852', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'25874', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26377', 
	'Medicamentos', 
	'Uso de medicamentos durante a amamentação', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26379', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26379', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26642', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26642', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26823', 
	'Medicamentos', 
	'Administração de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26896', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26896', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26905', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26905', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'26905', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27130', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27191', 
	'Medicamentos', 
	'Antídotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27197', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27197', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27216', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27383', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27497', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27548', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27553', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27553', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27665', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27665', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27703', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27703', 
	'Informação Toxicológica', 
	'Prevenção/Segurança/Informações educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27771', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27809', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27809', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27866', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'27900', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28153', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28160', 
	'Inseticidas de Uso Doméstico', 
	'Informações sobre inseticidas de uso doméstico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28181', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28189', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28220', 
	'Drogas de Abuso', 
	'Efeitos de substâncias ilícitas - sem vítima(s) identificada(s)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28249', 
	'Outros', 
	'Apoio para outros projetos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28266', 
	'Medicamentos', 
	'Administração de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28269', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28338', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28338', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28362', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28366', 
	'Informação Toxicológica', 
	'Primeiros socorros em intoxicações', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28372', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28372', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28434', 
	'Drogas de Abuso', 
	'Triagem toxicológica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28442', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28585', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'relação da toxicidade do SALOX na gestação.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28882', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28887', 
	'Medicamentos', 
	'Dose terapêutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28904', 
	'Produtos Domissanitários', 
	'Informações sobre produtos domissanitários', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28939', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28943', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28943', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28944', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28962', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28962', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28977', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'28992', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'29239', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'29239', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'29449', 
	'Administrativa', 
	'Informações sobre o Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'29791', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'29892', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Pcte queria saber diagnostico clinico.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'30386', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'30610', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'30912', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'30976', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'30976', 
	'Informação Ocupacional', 
	'Informações sobre os produtos químicos no local de trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31066', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31066', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31104', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31104', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31104', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31183', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31183', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31189', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31189', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31199', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31199', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31269', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31278', 
	'Drogas de Abuso', 
	'Triagem toxicológica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31441', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31441', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31448', 
	'Administrativa', 
	'Empréstimo de material didático', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31463', 
	'Informação Toxicológica', 
	'Referências bibliográficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31675', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31675', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31899', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31899', 
	'Administrativa', 
	'Empréstimo de material didático', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31995', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Informações sobre automedicação'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'31998', 
	'Medicamentos', 
	'Indicações/Uso terapêutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32000', 
	'Medicamentos', 
	'Antídotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32030', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32463', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32575', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32745', 
	'Medicamentos', 
	'Indicações/Uso terapêutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32745', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32779', 
	'Drogas de Abuso', 
	'Triagem toxicológica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'32863', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33358', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33358', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33372', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33372', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33373', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33451', 
	'Informação Toxicológica', 
	'Condutas terapêuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33473', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33526', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33526', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33526', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33747', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'vaselina líquida'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33775', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'33833', 
	'Medicamentos', 
	'Reações adversas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34072', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34142', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34250', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34275', 
	'Medicamentos', 
	'Reações adversas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34275', 
	'Medicamentos', 
	'Farmacologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34275', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34369', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	'Exames ocupacionais'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34376', 
	'Administrativa', 
	'Empréstimo de material didático', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34891', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Dados estatísticos de intoxicações por medicamentos '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'34901', 
	'Informação Toxicológica', 
	'Condutas terapêuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35161', 
	'Cosméticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informações sobre cosméticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35171', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35281', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35416', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35416', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35419', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35419', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35419', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35638', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35638', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'35911', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36115', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36134', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36134', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36237', 
	'Medicamentos', 
	'Farmacocinética', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36237', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Gadolinio - Meio de Contraste'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36398', 
	'Informação Ocupacional', 
	'Informação sobre Toxicologia Ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36408', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36418', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'36456', 
	'Produtos de Uso Veterinário', 
	'Informações sobre produtos de uso veterinário', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37062', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37112', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37112', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37112', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37209', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37209', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37209', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37440', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37440', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37445', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37445', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37545', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'estatística'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37552', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37893', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'37893', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38111', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38307', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38353', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38457', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38457', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38458', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38458', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38902', 
	'Informação Toxicológica', 
	'Doses tóxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38902', 
	'Medicamentos', 
	'Dose terapêutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38945', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'38945', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39052', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39052', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39058', 
	'Medicamentos', 
	'Administração de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39058', 
	'Medicamentos', 
	'Estabilidade/armazenamento', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39188', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39188', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39195', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39519', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Notificação compulsória de intoxicação por medicamentos'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'39552', 
	'Administrativa', 
	'Estatísticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40133', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40148', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40176', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40176', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40253', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40274', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	'Informação sobre o risco de evolui com síndrome alcalino lactea após o uso de Bicarbonato de sódio'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40318', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40334', 
	'Informação Toxicológica', 
	'Análise toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40337', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40437', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40437', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40441', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40441', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40445', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40446', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40446', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40544', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40564', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	'Solicitação de material educativo'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40564', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Solicitação de material educativo'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40582', 
	'Informação Toxicológica', 
	'Teratogenicidade', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40582', 
	'Informação Ocupacional', 
	'Informações sobre os produtos químicos no local de trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40628', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40747', 
	'Drogas de Abuso', 
	'Informações sobre drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40815', 
	'Identificação de Substâncias', 
	'Identificação de substâncias sem abuso conhecido', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40818', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40818', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40856', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40856', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40858', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40858', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40971', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40974', 
	'Medicamentos', 
	'Monitorização terapêutica de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'40984', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41009', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41026', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41026', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41038', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41038', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41138', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41239', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41239', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41607', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41662', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41717', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41717', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41826', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41887', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41887', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41887', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41890', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'41890', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42009', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42009', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42026', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42026', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42096', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42146', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	'Disponibilidade de SAEl'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42202', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42202', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42230', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42230', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42230', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42247', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42247', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42931', 
	'Plantas e Fungos', 
	'Primeiros socorros em acidentes com plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42931', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42932', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42995', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'42995', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43132', 
	'Cosméticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informações sobre cosméticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43137', 
	'Alimentos', 
	'Informações sobre alimentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43152', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43152', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43318', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43365', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43860', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'dados estatísticos nacionais'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43933', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Acidente com serpentes em cachorros.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43943', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43943', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43943', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'43966', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44074', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44074', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44141', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44194', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44240', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44374', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44537', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44569', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44635', 
	'Administrativa', 
	'Processos jurídicos (Ministério da Justiça/Ministério do Trabalho/Ministério Público)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44653', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44653', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44655', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44655', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44675', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'44675', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45100', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45172', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45172', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45211', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45211', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45211', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45401', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45401', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45438', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	'Fabio Porcaro liga e solicita informações sobre a planta anador/melhoral. Pois gostaria de fazer uso desta para fim medicinal (cefaléia). Informo que a planta é usada para este fim, mas para informar sobre o manejo e quantidade usada para diversas finalidades (chá, xarope...) demandaria maior pesquisa. Ele solicita que o CIT-Biologia envie e-mail para o seu endereço com tais informações.E-mail: fabioporcaro@hotmail.comDeixo o CIT à disposição.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45526', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45577', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45582', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45582', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45746', 
	'Medicamentos', 
	'Apresentação/formulação', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45902', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45902', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45955', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'45959', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46205', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Avaliar diagnóstico diferencial de lesão em face com 2 dias de evolução'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46261', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46307', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46333', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46333', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46338', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46342', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Harpagophytm procumbens - garra do diabo'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46461', 
	'Medicamentos', 
	'Administração de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46491', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46491', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46503', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46522', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46755', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46755', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46889', 
	'Administrativa', 
	'Estatísticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'46916', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47006', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47259', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47482', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47482', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47542', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47557', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47632', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47632', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47837', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'47918', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48047', 
	'Alimentos', 
	'Informações sobre alimentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48076', 
	'Informação Ocupacional', 
	'Informação sobre Toxicologia Ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48086', 
	'Medicamentos', 
	'Antídotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48103', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48131', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48134', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48134', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48151', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48151', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48271', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48302', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48305', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48305', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48308', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48308', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48343', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48343', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48351', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48351', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48351', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48480', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48750', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48750', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48812', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48831', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48831', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48855', 
	'Plantas e Fungos', 
	'Primeiros socorros em acidentes com plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'48855', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49014', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49014', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49032', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49047', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49047', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49048', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49048', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49067', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49263', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49446', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49451', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49451', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49493', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49493', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49493', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49624', 
	'Drogas de Abuso', 
	'Informações sobre drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'49655', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50063', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50132', 
	'Informação Toxicológica', 
	'Referências bibliográficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50503', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50503', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50527', 
	'Outros', 
	'Informações sobre outros intoxicantes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50685', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50685', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50858', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50995', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50995', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'50995', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51031', 
	'Plantas e Fungos', 
	'Identificação de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51406', 
	'Drogas de Abuso', 
	'Triagem toxicológica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51409', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51518', 
	'Inseticidas de Uso Doméstico', 
	'Informações sobre inseticidas de uso doméstico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51589', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51777', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51777', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51779', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51779', 
	'Administrativa', 
	'Estatísticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51806', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51823', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51998', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'51998', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'52221', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'52221', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'53678', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Monografia acidente botrópico'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'53843', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'53843', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'53892', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'54212', 
	'Administrativa', 
	'Outra informação administrativa', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55071', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55171', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55291', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55445', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55625', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55625', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55627', 
	'Medicamentos', 
	'Indicações/Uso terapêutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55633', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55633', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55642', 
	'Medicamentos', 
	'Antídotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55642', 
	'Administrativa', 
	'Outra informação administrativa', 
	'Informações sobre estoque e compra de antídotos.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55721', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55728', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55728', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55914', 
	'Medicamentos', 
	'Dose terapêutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'55917', 
	'Informação Toxicológica', 
	'Prevenção/Segurança/Informações educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56048', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56189', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56189', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56189', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56206', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56206', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56844', 
	'Administrativa', 
	'Empréstimo de material didático', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56848', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56848', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56848', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56885', 
	'Identificação de Substâncias', 
	'Identificação de substâncias geralmente envolvidas em abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56928', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'56947', 
	'Informação Toxicológica', 
	'Condutas terapêuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57064', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57064', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57129', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57129', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57504', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57504', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57529', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57529', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57757', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57782', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57811', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57811', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'57852', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'58255', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'58903', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'58903', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'58905', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'58905', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59074', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Envio de Lonomias ao I. Butantan'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59334', 
	'Medicamentos', 
	'Antídotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59358', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59407', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59600', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	'Tipos de veneno que pode ocorrer paralisia gradativa em cães'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59667', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59667', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59667', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'59970', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'60012', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'60764', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'60764', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'60816', 
	'Informação Toxicológica', 
	'Prevenção/Segurança/Informações educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'61185', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Envio de Lonômias ao CIT'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'61385', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Envio de Lonomias ao I.Butantan'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'61385', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'61595', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'61595', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'61610', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'61769', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Sobre a possibilidade de doenças e como coletar'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62308', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62345', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Teratogenicidade'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62533', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62574', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62578', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62784', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62784', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62808', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'62808', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63169', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63390', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63390', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63390', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63546', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63555', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63555', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'63555', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64061', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64212', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64342', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64389', 
	'Identificação de Substâncias', 
	'Identificação de substâncias sem abuso conhecido', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64428', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Informação sobre estatísticas de intoxicação por medicamentos.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64473', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Monografia'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64612', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64919', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64919', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'64934', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'65302', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'65302', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66793', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66793', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66797', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66803', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66803', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66838', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66838', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66985', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'66985', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67049', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67488', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67501', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67501', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67555', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67583', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67762', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67768', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67768', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67863', 
	'Alimentos', 
	'Informações sobre alimentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'67882', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68082', 
	'Drogas de Abuso', 
	'Informações sobre drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68138', 
	'Drogas de Abuso', 
	'Informações sobre drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68172', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68418', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68421', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68421', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68432', 
	'Medicamentos', 
	'Antídotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68626', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68626', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68658', 
	'Produtos de Uso Veterinário', 
	'Informações sobre produtos de uso veterinário', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68666', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68696', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68696', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68851', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'68851', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69252', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69252', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69274', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69533', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69560', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69563', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Informações '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69615', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69615', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69977', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69977', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'69977', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'70127', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'70144', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'70232', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'71164', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'71659', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'71697', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'71983', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	'Toxicidade'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'71993', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72106', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72241', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72389', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72398', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72400', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72400', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72405', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72405', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72461', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72818', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'72861', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'73040', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'73040', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'73040', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'73469', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'73713', 
	'Informação Toxicológica', 
	'Análise toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'73786', 
	'Inseticidas de Uso Doméstico', 
	'Informações sobre inseticidas de uso doméstico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74141', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74192', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74317', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74786', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74786', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74963', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74963', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74972', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'74972', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'75281', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'75281', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'75281', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'75286', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'75313', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'75360', 
	'Alimentos', 
	'Informações sobre alimentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'75552', 
	'Informação Toxicológica', 
	'Análise toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76073', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76073', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76439', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76439', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76439', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76494', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76494', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76494', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76544', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76615', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76667', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76667', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76711', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76711', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76711', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76897', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76981', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Recolhimento de cobras'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76981', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76981', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76982', 
	'Medicamentos', 
	'Uso de medicamentos durante a gravidez', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76997', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76997', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'76997', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77477', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77793', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77793', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77805', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77874', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77894', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77894', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77913', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'77913', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78044', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78170', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78170', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78375', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78375', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78641', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78641', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'78657', 
	'Informação Toxicológica', 
	'Análise toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79036', 
	'Plantas e Fungos', 
	'Identificação de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79036', 
	'Medicamentos', 
	'Indicações/Uso terapêutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79070', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79144', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79332', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79332', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79332', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79465', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79465', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79632', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79632', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'79747', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80170', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Solicitação feita pelo CIT, de coleta de Lonomias e posterior envio ao I. Butantan.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80180', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Encaminhamento de coleta de Lonomia e envio ao CIT e posterior encaminhamento ao I. Butantan'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80209', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80505', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80505', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80505', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80515', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80659', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'80672', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'81034', 
	'Medicamentos', 
	'Farmacocinética', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'81058', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'82378', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'82466', 
	'Administrativa', 
	'Processos jurídicos (Ministério da Justiça/Ministério do Trabalho/Ministério Público)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'82466', 
	'Administrativa', 
	'Estatísticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'82578', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83432', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83432', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83556', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83562', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83659', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83659', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83659', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'83834', 
	'Medicamentos', 
	'Uso de medicamentos durante a amamentação', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84133', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84133', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84133', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84258', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84261', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84263', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Principais medicamentos relacionados a intoxicação e óbito.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84269', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84269', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84273', 
	'Informação Toxicológica', 
	'Condutas terapêuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'84337', 
	'Informação Toxicológica', 
	'Condutas terapêuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85088', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85122', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85122', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85572', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85858', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85858', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85935', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'85935', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'86120', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'86164', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'86711', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'86711', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'86711', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'86919', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'87314', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'87314', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'87314', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'87315', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'87343', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'87625', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'88185', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Informações sobre tratamento de acidentes com animais peçonhentos'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'88185', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'88321', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'88489', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'88489', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'88769', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'88793', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89004', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	'Auxílio no esclarecimento de óbito.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89054', 
	'Outros', 
	'Apoio para outros projetos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89649', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89649', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89649', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89911', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89921', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'89921', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90144', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90149', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90149', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90351', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90351', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90359', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90359', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90395', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90395', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90548', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90548', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90568', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Como remover animais da residência.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90591', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90591', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90633', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'90633', 
	'Administrativa', 
	'Estatísticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'91330', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'91330', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'91444', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'91551', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'91860', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92456', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92700', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92700', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92735', 
	'Plantas e Fungos', 
	'Identificação de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92784', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92784', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92784', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92832', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'92962', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Coleta de Lonomias para envio ao Butantan'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93238', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93247', 
	'Outros', 
	'Informações sobre outros intoxicantes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93342', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93427', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93427', 
	'Produtos Domissanitários', 
	'Informações sobre produtos domissanitários', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93946', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93946', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93973', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93973', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93974', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'93974', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'94125', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'94278', 
	'Medicamentos', 
	'Uso de medicamentos durante a amamentação', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'94666', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'94682', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'94682', 
	'Medicamentos', 
	'Reações adversas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'94816', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95110', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95217', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95375', 
	'Administrativa', 
	'Empréstimo de material didático', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95375', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95375', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95407', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95407', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95495', 
	'Informação Toxicológica', 
	'Condutas terapêuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95559', 
	'Informação Ocupacional', 
	'Monitorização de toxicidade na rotina', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95560', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95560', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95634', 
	'Informação Toxicológica', 
	'Condutas terapêuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'95745', 
	'Administrativa', 
	'Estatísticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96660', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96661', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96662', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96760', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96760', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96780', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96795', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'96795', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'97007', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'97048', 
	'Informação Toxicológica', 
	'Condutas terapêuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'97325', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'97552', 
	'Alimentos', 
	'Informações sobre alimentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'98093', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'98119', 
	'Medicamentos', 
	'Administração de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'98414', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'98423', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'98714', 
	'Informação Toxicológica', 
	'Doses tóxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'98716', 
	'Informação Toxicológica', 
	'Análise toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'98926', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99205', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99205', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99229', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99231', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99319', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99319', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99672', 
	'Administrativa', 
	'Solicitação de 0800 para o rótulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99714', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'99714', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'100232', 
	'Medicamentos', 
	'Administração de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'100488', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'100541', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Solicita informações sobre transmissão de raiva por réptil.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'100793', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'101136', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'101136', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'101937', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'102012', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'102171', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'102542', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'102542', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'102691', 
	'Administrativa', 
	'Solicitação de 0800 para o rótulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'102984', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'103853', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'103853', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'103853', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'103933', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'103972', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104171', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104344', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104344', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104357', 
	'Informação Toxicológica', 
	'Análise toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104504', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104565', 
	'Drogas de Abuso', 
	'Informações sobre drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104579', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'conduta em caso de contato com morcego'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'104798', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'105649', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'105649', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'105660', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'105756', 
	'Medicamentos', 
	'Composição/Apresentação', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'105770', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106328', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106337', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106368', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106368', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106554', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106597', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106597', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106715', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106817', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106817', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106817', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106839', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'106910', 
	'Medicamentos', 
	'Apresentação/formulação', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'107130', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'107130', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'107248', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'107399', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'107399', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'107601', 
	'Medicamentos', 
	'Dose terapêutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'108595', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'108912', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'109103', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'109854', 
	'Informação Toxicológica', 
	'Prevenção/Segurança/Informações educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'109872', 
	'Informação Toxicológica', 
	'Primeiros socorros em intoxicações', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'109872', 
	'Informação Toxicológica', 
	'Prevenção/Segurança/Informações educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110071', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110071', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110071', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110112', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110112', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110116', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110116', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110116', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110369', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110695', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110746', 
	'Outros', 
	'Apoio para outros projetos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110746', 
	'Informação Toxicológica', 
	'Análise toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'110920', 
	'Administrativa', 
	'Informações sobre o Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'111725', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'111967', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112072', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112185', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112214', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112214', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112246', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112246', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112312', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112314', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112500', 
	'Administrativa', 
	'Solicitação de 0800 para o rótulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112530', 
	'Informação Ocupacional', 
	'Equipamentos de Proteção Coletiva (EPC)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'112530', 
	'Informação Ocupacional', 
	'Manipulação segura de substâncias químicas no trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'113504', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'113540', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'113860', 
	'Identificação de Substâncias', 
	'Identificação de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'114003', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'114252', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'114252', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'114462', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115003', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115050', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115101', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115747', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115747', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115747', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115928', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'115930', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'116232', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'116386', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'116386', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'116530', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'116530', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'116530', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'116891', 
	'Informação Toxicológica', 
	'Prevenção/Segurança/Informações educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'117136', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'117439', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'117439', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'117451', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'118062', 
	'Medicamentos', 
	'Indicações/Uso terapêutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'118201', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'118201', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'118201', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'118218', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'118756', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'119060', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'119150', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Informações sobre tratamento'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'119318', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'119347', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'119347', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'119627', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'120113', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'120791', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'120791', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'120909', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'120930', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'120930', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121022', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121022', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121024', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121029', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121029', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121034', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121058', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121304', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121332', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121629', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121629', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121633', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121668', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'121668', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122235', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122235', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122290', 
	'Informação Toxicológica', 
	'Prevenção/Segurança/Informações educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122526', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122533', 
	'Administrativa', 
	'Solicitação de 0800 para o rótulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122642', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122801', 
	'Produtos Domissanitários', 
	'Informações sobre produtos domissanitários', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122801', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'122842', 
	'Medicamentos', 
	'Farmacocinética', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'123158', 
	'Informação Toxicológica', 
	'Análise toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'123426', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'123426', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'123426', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'123669', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'124082', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Zuleika ligou para registrar que não é só em Canasvieiras que tem escorpião. No ano de 1990 menciona que morava em Ingleses, em uma casa de pedra no Morro do Maurício e coletou um escorpião que trouxe ao CIT/SC. À época disseram que até então nunca havia sido encontrado nenhum animal com essas características. Ligou para deixar esse registro.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'124131', 
	'Identificação de Substâncias', 
	'Identificação de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'124398', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'124433', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'124596', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'124829', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'124892', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'125240', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'125732', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Fornecimento de soro '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'125853', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'125853', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'125986', 
	'Identificação de Substâncias', 
	'Identificação de substâncias geralmente envolvidas em abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'125992', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'126145', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'126183', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'126183', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'126795', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'126795', 
	'Administrativa', 
	'Outra informação administrativa', 
	'Soro antielapídico e soro antibotrópico'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127555', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127555', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127572', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127662', 
	'Identificação de Substâncias', 
	'Identificação de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127789', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127789', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127789', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127870', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'127954', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128135', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128160', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128160', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128185', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128213', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128363', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128637', 
	'Identificação de Substâncias', 
	'Identificação de substâncias sem abuso conhecido', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128677', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128692', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'128733', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129070', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129285', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129381', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129389', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129446', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129481', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129749', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129749', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129954', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Conduta frente a diversos acidentes bothrópicos ocorridos em um mesmo paciente '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129966', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'129966', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'130040', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'130066', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'130184', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'130500', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131031', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131263', 
	'Inseticidas de Uso Doméstico', 
	'Informações sobre inseticidas de uso doméstico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131372', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131372', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131372', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131384', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131384', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131384', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131564', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131564', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131579', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131643', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131870', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131870', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131899', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'131910', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132140', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Analgésico p-hidroxi-benzoato de viminol (Zambon)'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132163', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132241', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132268', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132494', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132495', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132495', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'132684', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133179', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133233', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133303', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133303', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133326', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133326', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133326', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133787', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'133844', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134072', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Coleta e envio'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134072', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134072', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134074', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134074', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134079', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134079', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134371', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134384', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134998', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134998', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134998', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'134999', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135080', 
	'Administrativa', 
	'Solicitação de 0800 para o rótulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135085', 
	'Medicamentos', 
	'Composição/Apresentação', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135299', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135299', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135619', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135619', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135619', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135628', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135865', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'135894', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'136140', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'137607', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'137614', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'137614', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'137614', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'137668', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'137680', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'137792', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'138078', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'138089', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'138288', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'138288', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'138365', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'139120', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'139120', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'140902', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'140902', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'140986', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'140986', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141166', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141292', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141292', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141394', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141394', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141394', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141396', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141396', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'141585', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142422', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142422', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142422', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142422', 
	'Informação Toxicológica', 
	'Prevenção/Segurança/Informações educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142477', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142683', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142721', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'órgão responsável por recolher/coletar serpente dentro do domícilio'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142750', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	'Colinesterase alterada (11.328)'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'142750', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143047', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143047', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143050', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143050', 
	'Medicamentos', 
	'Reações adversas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143072', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143082', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143082', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143101', 
	'Medicamentos', 
	'Uso de medicamentos durante a gravidez', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143167', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143167', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143369', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143387', 
	'Plantas e Fungos', 
	'Identificação de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143387', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	'BENEFÍCIO DO USO DA PLANTA NO TRATAMENTO DO DIABETES'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143411', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'143825', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144617', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144617', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144625', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144625', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144628', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144628', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144628', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144875', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144875', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144886', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'144886', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145154', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145154', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145157', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145157', 
	'Produtos de Uso Veterinário', 
	'Informações sobre produtos de uso veterinário', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145200', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145200', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145535', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145535', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145636', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145789', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'145789', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'146198', 
	'Raticidas', 
	'Informações sobre raticidas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'146198', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'146476', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'146508', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'146904', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'146904', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147028', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147166', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147166', 
	'Informação Toxicológica', 
	'Prevenção/Segurança/Informações educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147166', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147446', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147446', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147461', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147461', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147735', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147735', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147963', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147963', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'147973', 
	'Informação Toxicológica', 
	'Doses tóxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'148409', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149047', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149104', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149670', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149670', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149688', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149688', 
	'Inseticidas de Uso Doméstico', 
	'Informações sobre inseticidas de uso doméstico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149832', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	'Investigaçãodiagnóstica'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149997', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'149998', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'150359', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'150609', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'150609', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151043', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151276', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151373', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151373', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151629', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151631', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151631', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151631', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151968', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'151968', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'152832', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'153607', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'153618', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'153899', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'153932', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'153932', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'153985', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Coleta de animais'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154013', 
	'Informação Toxicológica', 
	'Condutas terapêuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154032', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154032', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154034', 
	'Administrativa', 
	'Solicitação de 0800 para o rótulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154169', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154209', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Coleta de animais'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154402', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154402', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154402', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154557', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154662', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'154734', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155089', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155149', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155152', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155152', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155152', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155240', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155263', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155503', 
	'Drogas de Abuso', 
	'Triagem toxicológica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155527', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155632', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155663', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155665', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155680', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155680', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'155758', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156387', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156387', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156389', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156397', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156397', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156407', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156444', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Engano'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156474', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'156847', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Sobre critérios de ampolas adicionais com acidente crotálico, além de informações sobre consequências neurológicas como persistência de midríase e diplopia.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157179', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157208', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Prevalência e incidência de medicações tóxicas.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157241', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157360', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Acidente com perfuro cortantes em ambiente hospitalar'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157481', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157641', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157641', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157642', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157731', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157761', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157766', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157852', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157852', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'157977', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158122', 
	'Inseticidas de Uso Doméstico', 
	'Informações sobre inseticidas de uso doméstico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158305', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158470', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158476', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158506', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158512', 
	'Administrativa', 
	'Estatísticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158513', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158513', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'158764', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159122', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159152', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159449', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159449', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159487', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159501', 
	'Drogas de Abuso', 
	'Triagem toxicológica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159616', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159781', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159781', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'159848', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'160007', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Dados estatísticos de intoxicações por medicamentos.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'160702', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'160732', 
	'Informação Ocupacional', 
	'Informações sobre os produtos químicos no local de trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'160770', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'160809', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Mordida de animal não peçonhento'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'160961', 
	'Informação Toxicológica', 
	'Condutas terapêuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161470', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161540', 
	'Informação Ocupacional', 
	'Informação sobre Toxicologia Ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161541', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Captura de animais'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161598', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161772', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161772', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161821', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161821', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161821', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161851', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161855', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	'Conduta, prognóstico'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161921', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'161955', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162062', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162064', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'uso de argila branca'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162183', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162399', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162399', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162437', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162502', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162642', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162726', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162726', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162726', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162799', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162898', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'162898', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'163524', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	'Uso da Amora para a menopausa'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'163605', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'163733', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'orientação sobre mordedura canina'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'163819', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164128', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164141', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164181', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164356', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164378', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164387', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164643', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'164982', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165601', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165623', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165623', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165623', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165671', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Retirada de animal encontrado na residência.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165672', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165678', 
	'Medicamentos', 
	'Dose terapêutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165741', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165751', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165925', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Amostra enviada sem contato prévio com o CIT'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'165961', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'166219', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'166222', 
	'Administrativa', 
	'Solicitação de 0800 para o rótulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'166572', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'166572', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'MECANISMO DE AÇÃO'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'166748', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167383', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167547', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167547', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167566', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167566', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167566', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167631', 
	'Drogas de Abuso', 
	'Triagem toxicológica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167853', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Contato com gato'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'167910', 
	'Drogas de Abuso', 
	'Centros de referência para dependentes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168160', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168169', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168263', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168285', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	'Deseja saber sobre os usos de uma planta que leu ser moduladora celular (ajuda no câncer)'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168417', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Encaminhamento do paciente'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168467', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168467', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168727', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168736', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168749', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168749', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'168774', 
	'Informação Toxicológica', 
	'Teratogenicidade', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169400', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169400', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169498', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Medidas de profilaxia '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169581', 
	'Drogas de Abuso', 
	'Informações sobre drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169649', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'zika virus e dengue'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169675', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169875', 
	'Produtos Domissanitários', 
	'Informações sobre produtos domissanitários', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169875', 
	'Informação Toxicológica', 
	'Primeiros socorros em intoxicações', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'169970', 
	'Administrativa', 
	'Solicitação de 0800 para o rótulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170012', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170164', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170261', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170278', 
	'Raticidas', 
	'Informações sobre raticidas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170278', 
	'Informação Toxicológica', 
	'Referências bibliográficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170397', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170540', 
	'Inseticidas de Uso Doméstico', 
	'Informações sobre inseticidas de uso doméstico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170627', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170627', 
	'Informação Toxicológica', 
	'Primeiros socorros em intoxicações', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'170977', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171011', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171014', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171137', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171149', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171149', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171253', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171739', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171760', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171760', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171791', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'171839', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172120', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172428', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172429', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Informação sobre o atendimento do CIT'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172529', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172609', 
	'Medicamentos', 
	'Dose terapêutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172630', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172698', 
	'Informação Toxicológica', 
	'Referências bibliográficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172903', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'172910', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173240', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173332', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173332', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173361', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173681', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173686', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173686', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'173686', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174070', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174070', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174127', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174274', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174274', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174304', 
	'Informação Toxicológica', 
	'Prevenção/Segurança/Informações educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174307', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174382', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174822', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Uso de anti inflamatórios associados'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174874', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174933', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Animais peçonhentos no Alto Vale de SC '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'174993', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Se medicamento é confiavél'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175107', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175355', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175372', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175561', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175614', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175673', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175674', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175694', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175703', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'conduta'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175746', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175861', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175926', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'175926', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176071', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176344', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176344', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	'Toxicidade de gás de explosão de Bateria de no break'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176398', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176612', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176612', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176636', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176774', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176774', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176842', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176842', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176933', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'176961', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'177266', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'177401', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'177466', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'177473', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'177530', 
	'Informação Ocupacional', 
	'Informações sobre os produtos químicos no local de trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'177564', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'177606', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178101', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178101', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178328', 
	'Administrativa', 
	'Informações sobre o Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178496', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178496', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178577', 
	'Informação Toxicológica', 
	'Prevenção/Segurança/Informações educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178587', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178663', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178743', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Informações sobre dose tóxica '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178774', 
	'Produtos Domissanitários', 
	'Informações sobre produtos domissanitários', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178774', 
	'Informação Toxicológica', 
	'Primeiros socorros em intoxicações', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'178793', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179218', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179223', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179359', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Informação sobre uso de catárticos / carvão ativado em doses múltiplas'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179448', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	'Informação sobre toxicidade da fumaça de Parafina, Xilol e Formol'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179455', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179620', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179751', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Intoxicações com Tricíclicos e Benzodiazepínicos.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179764', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179764', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179764', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Cuidados para não ter acidente.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'179876', 
	'Produtos de Uso Veterinário', 
	'Informações sobre produtos de uso veterinário', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'180205', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'180351', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'180525', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Mordedura por cão'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'180541', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'181078', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'181078', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'181456', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Mordida de cachorro'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'181721', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'181819', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'toxicidade'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'182013', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'182081', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Habitat e hábitos'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'182340', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	'Chá de Palmeirinha - Eleutherine bulbosa'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'182592', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'182841', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'182852', 
	'Administrativa', 
	'Outra informação administrativa', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'183166', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'183181', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	'quer informações terapêuticas'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'183337', 
	'Medicamentos', 
	'Compatibilidade de medicação parenteral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'183505', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'183612', 
	'Medicamentos', 
	'Dose terapêutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'183924', 
	'Inseticidas de Uso Doméstico', 
	'Informações sobre inseticidas de uso doméstico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'183994', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'184549', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'184549', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'184597', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Soro antiofidico'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'184943', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'184943', 
	'Medicamentos', 
	'Antídotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185035', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185385', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185491', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185568', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185568', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185727', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185738', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185830', 
	'Outros', 
	'Informação sobre doenças gerais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'185891', 
	'Produtos Domissanitários', 
	'Informações sobre produtos domissanitários', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'186598', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Identificação de composição de fórmula'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'186626', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'186785', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'186785', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'187314', 
	'Informação Toxicológica', 
	'Referências bibliográficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'187368', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'187399', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'187779', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'187795', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'187813', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188273', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'sobre lote nª lote de salox aplicado a 4 anos em paciente.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188321', 
	'Raticidas', 
	'Informações sobre raticidas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188327', 
	'Drogas de Abuso', 
	'Triagem toxicológica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188362', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188446', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188446', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188507', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Preenchimento campo circunstância no DATATOX'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188661', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188750', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188752', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'188993', 
	'Administrativa', 
	'Outra informação administrativa', 
	'Informações sobre Soro'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'189012', 
	'Informação Ocupacional', 
	'Informação sobre Toxicologia Ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'189462', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190028', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190070', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190083', 
	'Inseticidas de Uso Doméstico', 
	'Informações sobre inseticidas de uso doméstico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190110', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Locais de atendimento'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190294', 
	'Medicamentos', 
	'Contra-indicações', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190635', 
	'Informação Toxicológica', 
	'Prevenção/Segurança/Informações educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190674', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Dose Tóxica'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190683', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190843', 
	'Informação Toxicológica', 
	'Doses tóxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190883', 
	'Drogas de Abuso', 
	'Triagem toxicológica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190916', 
	'Medicamentos', 
	'Interações fármaco-alimentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'190925', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'191076', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'192091', 
	'Outros', 
	'Informações sobre outros intoxicantes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'192614', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'192807', 
	'Informação Toxicológica', 
	'Condutas terapêuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'192877', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'193671', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'193717', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'193786', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'193890', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'194003', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'194300', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'194601', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'195316', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'195431', 
	'Medicamentos', 
	'Farmacocinética', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'195431', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'195458', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	'Protocolo de atendimento em intoxicação por raticidas'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'195848', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'195908', 
	'Identificação de Substâncias', 
	'Identificação de formas farmacêuticas sólidas (sob prescrição, isentas de prescrição, suplemento dietético/ervas, ilícita)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'196047', 
	'Medicamentos', 
	'Dose terapêutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'196395', 
	'Inseticidas de Uso Doméstico', 
	'Informações sobre inseticidas de uso doméstico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'196424', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'196775', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'196775', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'196845', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'197469', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'197475', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'197485', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Fitoterápicos'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'197612', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'197912', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'197913', 
	'Produtos Domissanitários', 
	'Informações sobre produtos domissanitários', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'198262', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'199211', 
	'Medicamentos', 
	'Indicações/Uso terapêutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'199221', 
	'Cosméticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informações sobre cosméticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'199427', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'199717', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'201051', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'201092', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'201441', 
	'Cosméticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informações sobre cosméticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'201468', 
	'Produtos de Uso Veterinário', 
	'Informações sobre produtos de uso veterinário', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'201718', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Mordedura de gato'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'201827', 
	'Medicamentos', 
	'Farmacologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'201847', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'202079', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'202138', 
	'Informação Toxicológica', 
	'Condutas terapêuticas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'202350', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'202377', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'202666', 
	'Medicamentos', 
	'Reações adversas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'202727', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'202727', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'203257', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'203474', 
	'Metais', 
	'Informações sobre metais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'203539', 
	'Outros', 
	'Informação sobre doenças gerais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'204236', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'204344', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'204381', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'204388', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Necessidade de condução para hospital'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'204992', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'204999', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'205786', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'205900', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'206043', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'206940', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'207311', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'207654', 
	'Cosméticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informações sobre cosméticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'207827', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'207827', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'207852', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'207922', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'208298', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	'Piper Longum - pode ser utilizada na gestação? 
Cadastrar. '
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'208872', 
	'Medicamentos', 
	'Uso de medicamentos durante a gravidez', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'208896', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'209742', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'209802', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'210375', 
	'Produtos Domissanitários', 
	'Informações sobre produtos domissanitários', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211079', 
	'Informação Toxicológica', 
	'Doses tóxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211192', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Dose tóxica'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211381', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211752', 
	'Administrativa', 
	'Informações sobre o Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211752', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211752', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211755', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211862', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'211862', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'212303', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'212564', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'213036', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'213296', 
	'Plantas e Fungos', 
	'Identificação de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'213296', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'213379', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'214257', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'214891', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'215086', 
	'Administrativa', 
	'Solicitação de 0800 para o rótulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'215130', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'215237', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'215455', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'215589', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'215589', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'215653', 
	'Identificação de Substâncias', 
	'Identificação de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'216415', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'216415', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'216415', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'216862', 
	'Produtos Domissanitários', 
	'Informações sobre produtos domissanitários', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'216947', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'216989', 
	'Medicamentos', 
	'Reações adversas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217428', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217434', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217693', 
	'Identificação de Substâncias', 
	'Identificação de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217769', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217773', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Como retirar o ferrão da abelha em caso de acidente'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217842', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217842', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217874', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'217886', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'219053', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'219182', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'219402', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'219656', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'219690', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'219690', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'220004', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'220004', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'220004', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'220062', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'220115', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'220328', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'221106', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'221255', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'221789', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'221789', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'222066', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'222235', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'223088', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'223138', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'223239', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'223448', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'223748', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'223775', 
	'Cosméticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informações sobre cosméticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'224283', 
	'Medicamentos', 
	'Composição/Apresentação', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'224532', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'224847', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225063', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225288', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225314', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225314', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225325', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225392', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225450', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225450', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225600', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225600', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225763', 
	'Informação Toxicológica', 
	'Análise toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225763', 
	'Informação Toxicológica', 
	'Referências bibliográficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'225799', 
	'Medicamentos', 
	'Administração de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'226317', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'226317', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'226936', 
	'Medicamentos', 
	'Composição/Apresentação', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'226936', 
	'Medicamentos', 
	'Apresentação/formulação', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'227177', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'227177', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'227182', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'227579', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'227938', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'227938', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'227938', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'228894', 
	'Informação Toxicológica', 
	'Referências bibliográficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'228903', 
	'Administrativa', 
	'Solicitação de 0800 para o rótulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'229527', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'229735', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'229754', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'229875', 
	'Medicamentos', 
	'Contra-indicações', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'229902', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230375', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230383', 
	'Medicamentos', 
	'Administração de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230430', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230430', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230810', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230840', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230843', 
	'Identificação de Substâncias', 
	'Identificação de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230848', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230902', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'230902', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'231115', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'231668', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'231678', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'232001', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'232210', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'232719', 
	'Medicamentos', 
	'Antídotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'233052', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'233052', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'233140', 
	'Plantas e Fungos', 
	'Identificação de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234281', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234281', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234331', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234336', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234639', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234640', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234640', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234684', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234846', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234846', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234868', 
	'Administrativa', 
	'Pesquisa na base de dados do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'234868', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'235070', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'235177', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'235678', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'235878', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'236382', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'236556', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'236556', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'236560', 
	'Informação Toxicológica', 
	'Doses tóxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'236838', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'236891', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'237016', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'237111', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'237431', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'237431', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'237736', 
	'Inseticidas de Uso Doméstico', 
	'Informações sobre inseticidas de uso doméstico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'238487', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'238487', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'238674', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'239101', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'239126', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'239301', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'239429', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'239429', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'239770', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'240557', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'240631', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'240987', 
	'Informação Toxicológica', 
	'Primeiros socorros em intoxicações', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'241201', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'241532', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'241569', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'241569', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'241569', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'241882', 
	'Medicamentos', 
	'Uso de medicamentos durante a amamentação', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'242343', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'242381', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'242383', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'242425', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'242634', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'242815', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'243078', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'243078', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'243251', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'243472', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'243729', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'244826', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'245617', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'245628', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'246054', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'246054', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'246203', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'246203', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'246283', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Soro loxoscélico antiveneno'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'247302', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'247465', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'247465', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'247847', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'247939', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'247939', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248156', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248177', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248211', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248211', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248318', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248318', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248518', 
	'Cosméticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informações sobre cosméticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248696', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'248915', 
	'Inseticidas de Uso Doméstico', 
	'Informações sobre inseticidas de uso doméstico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249045', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249063', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249063', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249074', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249074', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249088', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249088', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249104', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249139', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249153', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249181', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249207', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249337', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249491', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249492', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249503', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249503', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249518', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Como eliminar loxosceles'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249519', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249718', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Encaminhar lonomias'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249873', 
	'Medicamentos', 
	'Dose terapêutica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249893', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249893', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249916', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249926', 
	'Informação Ocupacional', 
	'Dados técnicos sobre segurança e manuseio de material (ficha técnica)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'249931', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250159', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250159', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250159', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250160', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250203', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250345', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250488', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250608', 
	'Informação Ocupacional', 
	'Informações sobre os produtos químicos no local de trabalho', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'250853', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'251244', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'251350', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'251700', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252047', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252051', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252051', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252400', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252712', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252712', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252803', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'252830', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'253834', 
	'Medicamentos', 
	'Reações adversas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'253931', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'253972', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'253972', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'253972', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254045', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254079', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254083', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254083', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254096', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254096', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254096', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254152', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254400', 
	'Administrativa', 
	'Estatísticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254407', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254407', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254500', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254500', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254750', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254759', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'254817', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'255838', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'255948', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'255948', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256130', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256194', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256194', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256203', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256203', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256241', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256855', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256934', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'256946', 
	'Informação Toxicológica', 
	'Referências bibliográficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'257098', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'257219', 
	'Medicamentos', 
	'Uso de medicamentos durante a gravidez', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'257309', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'257425', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'257494', 
	'Informação Toxicológica', 
	'Prevenção/Segurança/Informações educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'257598', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258034', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258080', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258142', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258156', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258308', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258494', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258683', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258693', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258851', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258865', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'258942', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'259185', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'259298', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'259579', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'259579', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'259629', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'259629', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'259629', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'260825', 
	'Identificação de Substâncias', 
	'Identificação de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'260869', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'260951', 
	'Outros', 
	'Informação sobre doenças gerais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'261125', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'261125', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'261545', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'261679', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	'Hepatotoxicidade'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'261682', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'261682', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'261899', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'262007', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'262007', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'262345', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'262345', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'262402', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'262402', 
	'Raticidas', 
	'Informações sobre raticidas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'262526', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263151', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263155', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263209', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Informação sobre realização de exames'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263245', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263272', 
	'Informação Ocupacional', 
	'Outra informação ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263272', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263347', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263347', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263525', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263525', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263525', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263579', 
	'Identificação de Substâncias', 
	'Identificação de outros produtos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263707', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263785', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263801', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263806', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263806', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263913', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'263913', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'264148', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'264148', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'264239', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'264239', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'265101', 
	'Drogas de Abuso', 
	'Triagem toxicológica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'265375', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'265674', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'265997', 
	'Medicamentos', 
	'Estabilidade/armazenamento', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'266444', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'266444', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'266801', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'266829', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Capturou animal e quer soltar'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'266852', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'267323', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'267323', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'267513', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Avaliar hipótese de exposição e conduta'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'267648', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'267818', 
	'Medicamentos', 
	'Reações adversas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268061', 
	'Medicamentos', 
	'Antídotos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268173', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268173', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268249', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268522', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268522', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268808', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268808', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'268816', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'269342', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'269709', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'269741', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'269744', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'270018', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'270018', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'270278', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'270450', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'270613', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'270613', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'271030', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'271030', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'271061', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'271074', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'271108', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'271762', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272234', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272330', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272566', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272566', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272732', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272812', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272825', 
	'Produtos Domissanitários', 
	'Informações sobre produtos domissanitários', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272882', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'272954', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'273165', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'273286', 
	'Informação Toxicológica', 
	'Doses tóxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'273286', 
	'Informação Toxicológica', 
	'Toxicidade geral', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'273286', 
	'Informação Toxicológica', 
	'Análise toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'273785', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'273952', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274063', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274165', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274165', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274165', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274166', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274166', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274166', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274497', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274497', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274497', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274672', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274756', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274756', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274756', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274786', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Conduta frente a acidente com abelhas'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'274876', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'275123', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'275145', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'275373', 
	'Informação Toxicológica', 
	'Prevenção/Segurança/Informações educativas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'275644', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'276187', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'276208', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'276361', 
	'Outros', 
	'Apoio para outros projetos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'276529', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'276859', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'277038', 
	'Administrativa', 
	'Entrevista com equipe do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'277041', 
	'Administrativa', 
	'Informações sobre o Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'277472', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'277608', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'278252', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'278485', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'278485', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'278744', 
	'Administrativa', 
	'Estatísticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'279200', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'279234', 
	'Informação Toxicológica', 
	'Outra informação toxicológica', 
	'Uso de pralidoxima no Brasil'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280090', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280222', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280222', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280229', 
	'Cosméticos, Produtos de Higiene Pessoal e Perfumes', 
	'Informações sobre cosméticos, produtos de higiene pessoal e perfumes', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280251', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280251', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280289', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280289', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280402', 
	'Plantas e Fungos', 
	'Identificação de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280402', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280676', 
	'Outros', 
	'Apoio para eventos não toxicológicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280803', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'280998', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281149', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281149', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281153', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281153', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281196', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281257', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281567', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281649', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'281649', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'282204', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'282204', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'282499', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'282499', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'282499', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'282656', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'282838', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Informações à respeito da toxicidade da Melaleuca para uso na agricultura.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'283047', 
	'Informação Toxicológica', 
	'Referências bibliográficas em toxicologia', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'283127', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'283470', 
	'Medicamentos', 
	'Uso de medicamentos durante a gravidez', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284204', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284204', 
	'Administrativa', 
	'Material informativo (folder, cartazes, etc.)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284264', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284350', 
	'Informação Toxicológica', 
	'Doses tóxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284482', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284619', 
	'Medicamentos', 
	'Outras informações sobre medicamentos', 
	'Protocolos de Atendimento em exposição a Barbitúricos.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284745', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284745', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284745', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284848', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284855', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284855', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284956', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284956', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'284998', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'285532', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'285656', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'285683', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'285969', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'285969', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'286089', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'286096', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'286247', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'286247', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'286435', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'286522', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'286875', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'287469', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'287733', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'288301', 
	'Administrativa', 
	'Solicitação de 0800 para o rótulo do produto', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'288339', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'288344', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'288370', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289050', 
	'Medicamentos', 
	'Indicações/Uso terapêutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289050', 
	'Medicamentos', 
	'Interações medicamentosas (sem exposição conhecida)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289241', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289288', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289355', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289355', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289540', 
	'Informação Toxicológica', 
	'Análise toxicológica', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289619', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289832', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289846', 
	'Agrotóxicos', 
	'Informações sobre agrotóxicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289954', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Coleta e descarte de substância desconhecida'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'289956', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'290240', 
	'Outros', 
	'Apoio para outros projetos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'290248', 
	'Outros', 
	'Outro tipo de informação não classificada', 
	'Informação sobre óbito por lonomia.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'290514', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'290877', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'291047', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'291487', 
	'Outros', 
	'Apoio para eventos não toxicológicos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'292051', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'292373', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'292373', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'292387', 
	'Informação Ocupacional', 
	'Informação sobre Toxicologia Ocupacional', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'292875', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'292875', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'292972', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'293199', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'293574', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'293574', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'293578', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'293783', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'293825', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'294065', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'294070', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'294070', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'294854', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'295067', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'295348', 
	'Administrativa', 
	'Processos jurídicos (Ministério da Justiça/Ministério do Trabalho/Ministério Público)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'295496', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'296477', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Primeiros socorros em animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'296479', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'296764', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	'Doação de aranha para disciplina'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'296845', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'297632', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Empréstimo de kit de animais peçonhentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'297721', 
	'Produtos Domissanitários', 
	'Informações sobre produtos domissanitários', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'297737', 
	'Raticidas', 
	'Informações sobre raticidas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'297795', 
	'Administrativa', 
	'Processos jurídicos (Ministério da Justiça/Ministério do Trabalho/Ministério Público)', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'297929', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Outra informação sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'298392', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'298392', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'298406', 
	'Medicamentos', 
	'Indicações/Uso terapêutico', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'299321', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'299413', 
	'Medicamentos', 
	'Administração de medicamentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'300213', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'300213', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'300219', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'300219', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'300230', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	'Uso, identificação, dose e toxicologia.'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'300663', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'301409', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'301409', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'302495', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'302495', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'302579', 
	'Produtos Químicos de Uso Residencial/Industrial', 
	'Informações sobre produtos químicos de uso residencial/industrial', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'302870', 
	'Administrativa', 
	'Estatísticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'302912', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'302912', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'303286', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'303286', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'303658', 
	'Administrativa', 
	'Palestra, aula, curso, seminário, simpósio, congresso, webconferência, etc. com profissional do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'303930', 
	'Informação Toxicológica', 
	'Doses tóxicas', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304015', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304063', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304063', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304297', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304297', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304297', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304757', 
	'Administrativa', 
	'Estatísticas do Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304823', 
	'Plantas e Fungos', 
	'Identificação de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304823', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'304851', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'305176', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'305354', 
	'Plantas e Fungos', 
	'Toxicidade de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'305354', 
	'Plantas e Fungos', 
	'Identificação de plantas e fungos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'305394', 
	'Administrativa', 
	'Outra informação administrativa', 
	'Envio de amostra para NARTAD'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'306474', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'306474', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'306474', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'306859', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'306859', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Recebimento de animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'306883', 
	'Administrativa', 
	'Visita ao Centro', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'307255', 
	'Outros', 
	'Apoio para outros projetos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'307267', 
	'Alimentos', 
	'Informações sobre alimentos', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'307390', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Informações sobre animais', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'307657', 
	'Plantas e Fungos', 
	'Outra informação sobre plantas e fungos', 
	'Informações sobre atividade terapêutica da planta'
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'307715', 
	'Drogas de Abuso', 
	'Triagem toxicológica para drogas de abuso', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'308048', 
	'Produtos Domissanitários', 
	'Informações sobre produtos domissanitários', 
	''
);

INSERT INTO intoxicacao_db.13_informacao (
	identificador_caso, 
	primeiro_nivel_informacao, 
	segundo_nivel_informacao, 
	dados_complementares
) VALUES (
	'308538', 
	'Animais Peçonhentos/Venenosos e Animais Não Peçonhentos/Não Venenosos', 
	'Identificação de animal', 
	''
);

