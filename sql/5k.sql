SELECT
	t01.identificador_caso AS ID,
	t01.via_exposicao AS VIA_EXP,
	t02.substancia_genero_especie AS SUBSTANCIA,
	t02.subclasse_agente AS AGENTE, 
	t03.grupo_tratamento AS TRATAMENTO
FROM
	05_exposicao t01
JOIN 06_agente_intoxicante t02 ON t01.identificador_caso = t02.identificador_caso
JOIN 08_tratamento t03 ON t02.identificador_caso = t03.identificador_caso
LIMIT 5000;