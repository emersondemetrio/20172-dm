CREATE TABLE IF NOT EXISTS intoxicacao_db.01_caso_intoxicacao (
	identificador_caso int,
	data_entrada datetime,
	grupo_ficha_coletiva varchar(1024),
	tipo_atendimento varchar(1024),
	classificacao_gravidade varchar(1024),
	manifestacao_clinica varchar(1024),
	informacoes_adicionais_tratamento text,
	caso_revisado varchar(1024),
	caso_validado varchar(1024)
);
CREATE TABLE IF NOT EXISTS intoxicacao_db.02_atendimento (
	identificador_caso int,
	local_atendimento varchar(1024),
	complemento_local_atendimento varchar(1024),
	meio_atendimento varchar(1024),
	data_atendimento datetime
);
CREATE TABLE IF NOT EXISTS intoxicacao_db.03_solicitante (
	identificador_caso int,
	categoria_solicitante varchar(1024),
	cidade_solicitacao varchar(1024),
	estado_solicitacao varchar(1024)
);
CREATE TABLE IF NOT EXISTS intoxicacao_db.04_paciente (
	identificador_caso int,
	idade varchar(1024),
	especificacao_idade varchar(1024),
	periodo_gestacao varchar(1024),
	peso varchar(1024),
	descricao varchar(1024),
	cidade_residencia varchar(1024),
	estado_residencia varchar(1024),
	profissao varchar(1024),
	sexo varchar(1024),
	raca_etnia varchar(1024),
	data_nascimento datetime
);
CREATE TABLE IF NOT EXISTS intoxicacao_db.05_exposicao (
	identificador_caso int,
	data_exposicao datetime,
	horario_exposicao varchar(1024),
	tempo_decorrido varchar(1024),
	especificacao_tempo_decorrido varchar(1024),
	tempo_exposicao varchar(1024),
	especificacao_tempo_exposicao varchar(1024),
	intensidade_exposicao varchar(1024),
	local_exposicao varchar(1024),
	zona_exposicao varchar(1024),
	cidade_exposicao varchar(1024),
	estado_exposicao varchar(1024),
	pais_exposicao varchar(1024),
	via_exposicao varchar(1024),
	circunstancia_exposicao varchar(1024),
	intencao_exposicao varchar(1024)
);
CREATE TABLE IF NOT EXISTS intoxicacao_db.06_agente_intoxicante (
	identificador_caso int,
	nome_popular_comercial varchar(1024),
	substancia_genero_especie varchar(1024),
	subclasse_agente varchar(1024),
	classe_agente varchar(1024),
	grupo_agente varchar(1024),
	parte_corpo varchar(1024),
	quantidade_agente varchar(1024),
	especificacao_quantidade_agente varchar(1024),
	dose_exposicao varchar(1024),
	especificacao_dose_exposicao varchar(1024)
);
CREATE TABLE IF NOT EXISTS intoxicacao_db.07_manifestacao (
	identificador_caso int,
	manifestacao_apresentada varchar(1024),
	classificacao_manifestacao varchar(1024),
	complemento_manifestacao varchar(1024)
);
CREATE TABLE IF NOT EXISTS intoxicacao_db.08_tratamento (
	identificador_caso int,
	grupo_tratamento varchar(1024),
	subgrupo_tratamento varchar(1024),
	medida_tomada varchar(1024),
	medida_orientada varchar(1024),
	medida_realizada varchar(1024),
	medida_ignorada varchar(1024),
	nome_comercial_produto varchar(1024),
	quantidade varchar(1024),
	dose_produto varchar(1024),
	numero_lote varchar(1024),
	data_validade datetime,
	nome_fabricante varchar(1024),
	tipo_anestesico varchar(1024),
	numero_vezes varchar(1024),
	data_soroterapia datetime,
	hora_soroterapia varchar(1024),
	numero_lote1 varchar(1024),
	data_validade1 datetime,
	fabricante_soroterapia varchar(1024),
	numero_ampolas_iniciais varchar(1024)
);
CREATE TABLE IF NOT EXISTS intoxicacao_db.09_exame (
	identificador_caso int,
	descricao_tipo_exame varchar(1024),
	descricao_modalidade varchar(1024),
	tipo_resultado varchar(1024),
	valor_referencia_inferior varchar(1024),
	valor_referencia_superior varchar(1024),
	valor_referencia_textual varchar(1024),
	unidade_medida varchar(1024),
	especificacao_resultado_qualitativo varchar(1024),
	tipo_amostra varchar(1024),
	data_exame datetime,
	resultado_quantitativo varchar(1024),
	desfecho_resultado_quantitativo varchar(1024),
	resultado_qualitativo varchar(1024),
	resultado_textual varchar(1024),
	complemento varchar(1024)
);
CREATE TABLE IF NOT EXISTS intoxicacao_db.12_encerramento (
	identificador_caso int,
	data_encerramento datetime,
	internacao varchar(1024),
	tempo_internacao varchar(1024),
	especificacao_tempo_internacao varchar(1024),
	local_internacao varchar(1024),
	motivo_nao_internacao varchar(1024),
	desfecho varchar(1024),
	data_obito datetime,
	contribuicao_obito varchar(1024),
	autopsia varchar(1024),
	resultado_autopsia varchar(1024),
	cid10 varchar(1024),
	observacao varchar(1024)
);
CREATE TABLE IF NOT EXISTS intoxicacao_db.13_informacao (
	identificador_caso int,
	primeiro_nivel_informacao varchar(1024),
	segundo_nivel_informacao varchar(1024),
	dados_complementares varchar(1024)
);
CREATE TABLE IF NOT EXISTS intoxicacao_db.14_paciente_animal (
	identificador_caso int,
	data_nascimento datetime,
	idade varchar(1024),
	especificacao_idade varchar(1024),
	sexo varchar(1024),
	peso varchar(1024),
	cidade_residencia varchar(1024),
	estado_residencia varchar(1024),
	pais_residencia varchar(1024),
	especie varchar(1024),
	complemento_especie varchar(1024),
	raca varchar(1024),
	complemento_raca varchar(1024)
);
CREATE TABLE IF NOT EXISTS intoxicacao_db.99_banco_agentes_intoxicantes (
	grupo varchar(1024),
	classe varchar(1024),
	subclasse varchar(1024),
	substancia varchar(1024),
	nome_popular_comercial varchar(1024)
);
