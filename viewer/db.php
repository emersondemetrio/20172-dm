<?php 

function get_data($ID_CASO) {

	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "intoxicacao_db";

	$tables = array(
		"01_caso_intoxicacao" => "SELECT 
		data_entrada,
		grupo_ficha_coletiva, 
		tipo_atendimento, 
		classificacao_gravidade,
		manifestacao_clinica, 
		informacoes_adicionais_tratamento, 
		caso_revisado, 
		caso_validado 
		FROM 01_caso_intoxicacao WHERE identificador_caso = ",

		"02_atendimento" => "SELECT 
		local_atendimento,
		meio_atendimento
		FROM 02_atendimento WHERE identificador_caso = ",

		"03_solicitante" => "SELECT 
		categoria_solicitante,
		cidade_solicitacao,
		estado_solicitacao 
		FROM 03_solicitante WHERE identificador_caso = ",

		"04_paciente" => "SELECT 
		idade,
		especificacao_idade,
		periodo_gestacao,
		peso,
		descricao,
		cidade_residencia,
		estado_residencia,
		profissao,
		sexo,
		raca_etnia,
		data_nascimento 
		FROM 04_paciente WHERE identificador_caso = ",

		"05_exposicao" => "SELECT 
		horario_exposicao,
		tempo_decorrido,
		especificacao_tempo_decorrido,
		especificacao_tempo_exposicao,
		intensidade_exposicao,
		local_exposicao,
		zona_exposicao,
		cidade_exposicao,
		estado_exposicao,
		pais_exposicao,
		via_exposicao,
		circunstancia_exposicao,
		intencao_exposicao 
		FROM 05_exposicao WHERE identificador_caso = ",

		"06_agente_intoxicante" => "SELECT 
		substancia_genero_especie,
		subclasse_agente,
		classe_agente,
		grupo_agente,
		especificacao_quantidade_agente,
		especificacao_dose_exposicao
		FROM 06_agente_intoxicante WHERE identificador_caso = ",


		"07_manifestacao" => "SELECT 
		manifestacao_apresentada,
		classificacao_manifestacao
		FROM 07_manifestacao WHERE identificador_caso = ",

		"08_tratamento" => "SELECT 
		grupo_tratamento, 
		subgrupo_tratamento, 
		medida_tomada, 
		medida_orientada, 
		medida_realizada, 
		medida_ignorada, 
		numero_lote1, 
		numero_ampolas_iniciais
		FROM 08_tratamento WHERE identificador_caso = ",

		"09_exame" => "SELECT 
		descricao_tipo_exame,
		descricao_modalidade,
		tipo_resultado,
		valor_referencia_textual,
		tipo_amostra,
		data_exame
		FROM 09_exame WHERE identificador_caso = ",

		"12_encerramento" => "SELECT 
		data_encerramento,
		internacao,
		motivo_nao_internacao,
		desfecho,
		data_obito 
		FROM 12_encerramento WHERE identificador_caso = ",
		"13_informacao" => "SELECT * FROM 13_informacao WHERE identificador_caso = "
	);

	$conn = new mysqli($servername, $username, $password, $dbname);
	$conn->set_charset("utf8");

	if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
	} 

	$query_results = array();	

	foreach ($tables as $table_name => $query) {
		$sql = $query . $ID_CASO . " LIMIT 1";

		$result = $conn->query($sql);
		
		if (is_object($result)) {
			if ($result->num_rows > 0) {
				while($row = $result->fetch_assoc()) {
					$query_results[$table_name][] = $row;
				}
			}
		}
	}

	$conn->close();
	return $query_results;
}

?>