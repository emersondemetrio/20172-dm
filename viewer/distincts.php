<?php 

$table = $_GET['table'];
$col = $_GET['col'];

$sql = "SELECT DISTINCT($col) FROM $table;";

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "intoxicacao_db";

$conn = new mysqli($servername, $username, $password, $dbname);
$conn->set_charset("utf8");

if (!$conn) {
	die("Connection failed: " . mysqli_connect_error());
} 

$query_results = array();	
$result = $conn->query($sql);

if (is_object($result)) {
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$query_results[] = $row[$col];
		}
	}
}

$conn->close();
echo json_encode($query_results);
?>