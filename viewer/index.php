<?php 
require_once("db.php"); 
require_once("years.php"); 


$year = 2016;
if (isset($_GET['year'])) {
  $year = $_GET['year'];
}

$cases = get_cases_ids($year); 

$result = array();
$case = "";

if (isset($_GET['case'])) {
  $case = $_GET['case'];
  $result = get_data($case);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>DT Show Case</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    .container-full { margin: 10px auto; width: 100%; padding-top: 20px; }
    .show-modal { cursor: pointer; }
    .modal-body { max-height: 500px; overflow-y: scroll;}
    table tr td { font-size: 12px; }
  </style>

  <script>
    let SELECTED_YEAR = "<?php echo $year; ?>";
    let SELECTED_CASE = "<?php echo $case; ?>";
  </script>
</head>

<body>

  <div class="col-md-12 text-center"> 
    <h2>Data Tox Show Case</h2>
    <br>
    <form class="form-inline">
      <div class="form-group">
      <label for="pwd">ANO:</label>
        <select id="selYear" class="form-control" name="year">
          <?php foreach (range(2012, date('Y')) as $sel_year) {
            $selected = $sel_year == $year ? "selected" : "";
            echo "<option $selected value='$sel_year'>$sel_year</option>";
          } ?>
        </select>
      </div>

      <div class="form-group">
        <label for="pwd">IDENTIFICADOR DO CASO:</label>
        <select class="form-control" id="selCaseId" name="case">
          <option></option>
          <?php ?>
          <?php foreach ($cases as $key => $sel_case) {
            $selected = $case == $sel_case ? "selected" : "";
            echo "<option $selected value='$sel_case'>$sel_case</option>";
          } ?>
        </select>
      </div>  
    </form>
    <br>
  </div>


  <div class="container-full">
    <div class="col-md-12"> 
      <?php if(isset($case) && $case !== ""){ ?>
      <h3 class="text-center">CASO: <strong><?php echo trim($case); ?></strong></h3>
      <?php } ?>

      <?php $line_counter = 0; ?>
      <?php foreach ($result as $table_name => $result_set) { ?>

      <div class="col-md-4">  
        <h3 class="text-center"><?php echo trim($table_name); ?></h3>

        <table class="table">
          <?php foreach ($result_set as $key => $table) { ?>
          <?php foreach ($table as $col_name => $col_value) { ?>
          <?php if ($col_value !== "") { ?>
          <tr>            
            <td>
              <strong class="show-modal" data-col="<?php echo $col_name;?>" data-tablen="<?php echo $table_name;?>">
                <?php echo trim($col_name); ?>
              </strong>
            </td>
            <td><?php echo trim($col_value); ?></td>
          </tr>
          <?php } ?>
          <?php } ?>
          <?php } ?>
        </table>
      </div>

      <?php $line_counter++; ?>
      <?php if ($line_counter > 2) { ?>
      <?php $line_counter = 0; ?>
      <div class="col-md-12">
        <hr>
      </div>
      <?php } ?>
      <?php } ?>

      <?php if (count($result) == 0 && isset($case) && $case != "") { ?> 
        <h4 class='text-center'>Nenhum resultado encontrado.</h4>
      <?php } ?>

      <?php if( !isset($case) || $case == "") { ?>
        <h4 class='text-center'>Escolha um caso.</h4>
      <?php } ?>
    </div>
  </div>


  <div class="modal fade" id="dtModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center"><span id="table-title"></span>.<span id="col-title"></span></h4>
        </div>
        <div class="modal-body"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
        </div>
      </div>
      
    </div>
  </div>

  <script src="index.js"></script>
</body>
</html>
