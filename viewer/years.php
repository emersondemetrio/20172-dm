<?php 

function get_cases_ids($base = 2016) {

	$min = "'$base-01-01'";
	$max = "'" . ($base +1) ."-01-01'";
	
	$sql = "SELECT DISTINCT(identificador_caso) FROM 01_caso_intoxicacao WHERE data_entrada > $min AND data_entrada < $max;";

	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "intoxicacao_db";

	$conn = new mysqli($servername, $username, $password, $dbname);
	$conn->set_charset("utf8");

	if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
	} 

	$query_results = array();	

	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$query_results[] = $row['identificador_caso'];
		}
	}

	$conn->close();
	return $query_results;
}

?>