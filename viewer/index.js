$(() => {
	$("#selCaseId").on("change", function() {
		let caseId = $(this).val();
		if (caseId && caseId !== "") {
			$(".form-inline").submit();
		}
	});

	$("#selYear").on("change", function() {
		let year = $(this).val();
		if (year && year !== "") {
			$(".form-inline").submit();
			window.location = `?year=${year}`;
		}
	});

	$(".show-modal").on("click", function() {
		let tableName = $(this).attr("data-tablen");
		let colName = $(this).attr("data-col");
		let url = `distincts.php?table=${tableName}&col=${colName}`;
		
		$.get(url, (rawData) => {
			let resp = JSON.parse(rawData);
			if (resp) {
				let htmlData = "<ul>";
				resp.forEach((element) => {
					if (element == "") {
						element = "(Nenhum Valor)";
					}
					htmlData += `<li>${element}</li>`;
				});
				$(".modal-body").html(htmlData + "</ul>");
				$("#table-title").text(tableName);
				$("#col-title").text(colName);
				$("#dtModal").modal("show");
			}
		});
	});
});