#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Read and Parse info
"""

from __future__ import print_function
import os
import argparse
import csv
import re

def main():
    """main"""

    desc = "This module reads the information from given folder."
    parser = argparse.ArgumentParser(description=desc)

    print("\n[ INFO EXTRACTOR ]\n%s\n" % desc)

    parser.add_argument('-i', '--input_folder', required=True)
    parser.add_argument('-o', '--output_folder', required=True)
    parser.add_argument('-d', '--db_prefix', default="intoxicacao_db")

    parsed = parser.parse_args()

    create_sql_from_csv(
        parsed.input_folder,
        parsed.output_folder,
        parsed.db_prefix
    )

def create_sql_from_csv(full_path, output_folder, db_prefix):
    """
    Reading Information in Folder
    """
    full_out_folder = os.path.abspath(output_folder)

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    count = 0
    max_files = 100

    for root, _dirs, files in os.walk(full_path):
        for file_name in files:

            if ".txt" in file_name and count <= max_files:
                extract_file_data(
                    os.path.abspath(root),
                    file_name,
                    full_out_folder,
                    db_prefix
                )
            count += 1

def get_column_type(col):
    """get column type"""

    if "identificador" in col:
        return "int"

    if "data" in col:
        return "datetime"

    if "adicionais" in col:
        return "text"

    return "varchar(1024)"

def safe_string(inpt):
    """scape string"""

    if inpt == "'":
        return "''"

    return "'{}'".format(
                inpt.replace("\\", " ")
                .replace("'", " minutos ")
                .replace('"', " segundos")
            )

def create_insert_stm(table_name, header, row, db_prefix):
    """create sql insert statement"""

    sql = "INSERT INTO {}.{} (\n{}\n) VALUES (\n{}\n);".format(
        db_prefix,
        table_name,
        ", \n".join("\t" + col for col in header),
        ", \n".join("\t" + safe_string(val) for val in row)
    )

    return sql

def create_create_stm(table_name, header, db_prefix):
    """create sql create statement"""

    sql = "CREATE TABLE IF NOT EXISTS {}.{} (\n{}\n);".format(
        db_prefix,
        table_name,
        ",\n".join("\t" + col + " " + get_column_type(col) for col in header),
    )

    return sql


def create_sql_create_file(file_name, table_name, header, db_prefix):
    """Create sql file with create sttm"""

    with open(file_name, 'a') as the_file:
        the_file.write(
            create_create_stm(table_name, header, db_prefix)
        )
        the_file.write("\n")

def extract_file_data(base_dir, input_file, output_folder, db_prefix):
    """Read TXT files"""

    full_path = os.path.join(base_dir, input_file)

    with open(full_path, 'rb') as csvfile:
        rows = csv.reader(csvfile, delimiter='|', quotechar='"')

        create_table_file = os.path.join(
            output_folder,
            "create_{}".format(
                input_file.replace("txt", "sql")
            )
        )

        inserts_file = os.path.join(
            output_folder,
            "insert_{}".format(
                input_file.replace("txt", "sql")
            )
        )

        table_name = input_file.replace(".txt", "")

        with open(inserts_file, 'a') as inst_file:
            count = 0
            header = ""

            for row in rows:
                if count == 0:
                    header = row
                    create_sql_create_file(
                        create_table_file,
                        table_name,
                        row,
                        db_prefix
                    )
                else:
                    inst_file.write(
                        "{}\n".format(
                            create_insert_stm(
                                table_name,
                                header,
                                row,
                                db_prefix
                            )
                        )
                    )
                    inst_file.write("\n")
                count += 1

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Bye.")
