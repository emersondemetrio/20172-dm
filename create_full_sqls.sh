#!/usr/bin/env bash

rm sql/*;

cat output/create* > sql/full_create.sql;
cat output/insert* > sql/full_insert.sql;
